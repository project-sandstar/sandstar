
**
** analogValuePoint
**   Defines native functions to interact with digital outputs
**

class analogValuePoint
{
  static native bool set(int channel, float value);
}

@niagaraIcon="module://icons/x16/control/numericPoint.png" 
public class AnalogValue extends Component {
  
  ////////////////////////////////////////////////////////////////
  // Properties
  ////////////////////////////////////////////////////////////////
  
  ** The virtual channel name.
  @readonly @asStr property Buf(50) virtualChName = ""
  
  ** The virtual channel number.
  @readonly property int virtualCh = 0;
  
  ** Analog Value
  @readonly property float out = 0.0f;
  
  ** Input Trigger
  @config @summary=false property float in1 = null;
  @config @summary=false property float in2 = null;
  @config @summary=false property float in3 = null;
  @config @summary=false property float in4 = null;
  @config @summary=false property float in5 = null;
  @config @summary=false property float in6 = null;
  @config @summary=false property float in7 = null;
  @config @summary=false property float in8 = null;
  @config @summary=false property float in9 = null;
  @config @summary=false property float in10 = null;
  @config @summary=false property float in11 = null;
  @config @summary=false property float in12 = null;
  @config @summary=false property float in13 = null;
  @config @summary=false property float in14 = null;
  @config @summary=false property float in15 = null;
  @config @summary=false property float in16 = null;
  @config property float relinquishDefault = 0.0f;
  
  ** Marker List
  @config @asStr property Buf(100) pointQuery;
  
  ** Record Count
  @readonly property int pointQuerySize;
  
  ** point query status. 
  @allowNull=false
  @trueText="ok" @falseText="fault"
  @readonly property bool pointQueryStatus;
  
  ** Temporary Marker List
  inline Str(100) tempPointQuery = "nan";
  
  ** Current Status on channel
  @readonly @asStr property Buf(20) curStatus = "na";

  ** Operating Status
  @allowNull=false
  @trueText="yes" @falseText="no"
  @readonly property bool enabled;
  
  
  ////////////////////////////////////////////////////////////////
  // Class Global
  ////////////////////////////////////////////////////////////////
  private long ticks;
  
  ////////////////////////////////////////////////////////////////
  // Action Methods 
  ////////////////////////////////////////////////////////////////

  action void query()
  {
    tempPointQuery.copyFromStr(pointQuery.toStr(), 100);
		
	// Count total records that contains similar pointQuery
	pointQuerySize := eacio.getRecordCount(pointQuery.toStr());
	
	pointQueryStatus := (pointQuerySize == 1) ? (true) : (false);
	
	// Get channel number
	virtualCh := eacio.resolveChannel(pointQuery.toStr());
	
	if (virtualCh != 0)
	{
	  //Enabled?
	  enabled := eacio.isChannelEnabled(virtualCh);
	  
	  // Update sedonaId and sedonaType tag
	  eacio.writeSedonaId(virtualCh, this.id);
	  eacio.writeSedonaType(virtualCh, this.type.kit.name, this.name);
	  
	  // Update channel name
	  // FIX: Updating Buf in native code do not update the Buf member 'size'.
	  // Below fix updates the size of Buf according to size of string.
	
	  eacio.getChannelName(virtualCh, virtualChName.toStr());
	  virtualChName.copyFromStr(virtualChName.toStr());
	  changed(AnalogValue.virtualChName);
	
	  out := relinquishDefault;
	}
  }
  
  ////////////////////////////////////////////////////////////////
  // Override Methods
  ////////////////////////////////////////////////////////////////
  
  virtual override void start()
  {
	ticks = Sys.ticks();
  }
  
  virtual override void execute() 
  { 
    // TODO: As per SedonaDev documentation, No guarantee is made whether the 
	// string (returned by toStr()) is actually null terminated.
	
    if(!tempPointQuery.equals(pointQuery.toStr()))
	{
	  tempPointQuery.copyFromStr(pointQuery.toStr(), 100);
	  
	  // Count total records that contains similar pointQuery
	  pointQuerySize := eacio.getRecordCount(pointQuery.toStr());
	  
	  if (pointQuerySize == 1)
	  {
	    pointQueryStatus := true;
	  }
	  else
	  {
	    pointQueryStatus := false;
	  }
		
	  // Get channel number
	  virtualCh := eacio.resolveChannel(pointQuery.toStr());
	
	  if (virtualCh != 0)
	  {
	    //Enabled?
	    enabled := eacio.isChannelEnabled(virtualCh);
		
	    // Update sedonaId and sedonaType tag
	    eacio.writeSedonaId(virtualCh, this.id);
	    eacio.writeSedonaType(virtualCh, this.type.kit.name, this.name);
	  
		// Update channel name
		// FIX: Updating Buf in native code do not update the Buf member 'size'.
		// Below fix updates the size of Buf according to size of string.
	
		eacio.getChannelName(virtualCh, virtualChName.toStr());
		virtualChName.copyFromStr(virtualChName.toStr());
		changed(AnalogValue.virtualChName);
		
		out := relinquishDefault;
	  }
	}
	
	//Update curStatus every interval
	if (Sys.ticks() > (ticks + 2sec))
	{
	  // Update for next trigger
	  ticks = Sys.ticks();
	  
	  if(virtualCh!=0)
	  {
	    // enabled slot
	    enabled := eacio.isChannelEnabled(virtualCh);
		
	    // curStatus
	    eacio.getCurStatus(virtualCh, curStatus.toStr());
	    curStatus.copyFromStr(curStatus.toStr());
	    changed(AnalogValue.curStatus);
		
		//Level
		int level=0;
	    float inValue;
	    
	    level=eacio.getLevel(virtualCh);
	    if(level!=0)
	    {
		  //TODO: How to determine whether value is null or 0.0,
		  // if you receive 0.0 as input?
	      inValue=eacio.getLevelValue(virtualCh,level);
		  
		  //TODO: Below is magic number and Important. Keep insync with Haystack code
		  if(inValue<=-8888f)
		  {
		    inValue=null;
		  }
		  
		  switch(level)
		  {
		    case 1:
		      in1:=inValue;
		  	break;
		    case 2:
		      in2:=inValue;
		  	break;
		    case 3:
		      in3:=inValue;
		  	break;
		    case 4:
		      in4:=inValue;
		  	break;
		    case 5:
		      in5:=inValue;
		  	break;
		    case 6:
		      in6:=inValue;
		  	break;
		    case 7:
		      in7:=inValue;
		  	break;
		    case 8:
		      in8:=inValue;
		  	break;
		    case 9:
		      in9:=inValue;
		  	break;
		    case 10:
		      in10:=inValue;
		  	break;
		    case 11:
		      in11:=inValue;
		  	break;
		    case 12:
		      in12:=inValue;
		  	break;
		    case 13:
		      in13:=inValue;
		  	break;
		    case 14:
		      in14:=inValue;
		  	break;
		    case 15:
		      in15:=inValue;
		  	break;
		    case 16:
		      in16:=inValue;
		  	break;
		    case 17:
		      relinquishDefault:=inValue;
		  	break;
		  }
		  
		  //This is fix. When any input is set to null changed function is not called. Why?
		  //TODO: FIx this.
		  if((in1 == null) && (in2 == null) && (in3 == null) && (in4 == null) && (in5 == null) && (in6 == null) && (in7 == null) && (in8 == null) && (in9 == null) && (in10 == null) && (in11 == null) && (in12 == null) && (in13 == null) && (in14 == null) && (in15 == null) && (in16 == null))
  		  {
		    out := relinquishDefault;
		    analogValuePoint.set(virtualCh, relinquishDefault);
		  }
	    }
	  }
	}
  }
  
  override void setToDefault(Slot slot) 
  {
	super.setToDefault(slot);
	
	if(slot.name == "in1")
	{
	  in1 := null;
	}
	else if(slot.name == "in2")
	{
	  in2 := null;
	}
	else if(slot.name == "in3")
	{
	  in3 := null;
	}
	else if(slot.name == "in4")
	{
	  in4 := null;
	}
	else if(slot.name == "in5")
	{
	  in5 := null;
	}
	else if(slot.name == "in6")
	{
	  in6 := null;
	}
	else if(slot.name == "in7")
	{
	  in7 := null;
	}
	else if(slot.name == "in8")
	{
	  in8 := null;
	}
	else if(slot.name == "in9")
	{
	  in9 := null;
	}
	else if(slot.name == "in10")
	{
	  in10 := null;
	}
	else if(slot.name == "in11")
	{
	  in11 := null;
	}
	else if(slot.name == "in12")
	{
	  in12 := null;
	}
	else if(slot.name == "in13")
	{
	  in13 := null;
	}
	else if(slot.name == "in14")
	{
	  in14 := null;
	}
	else if(slot.name == "in15")
	{
	  in15 := null;
	}
	else if(slot.name == "in16")
	{
	  in16 := null;
	}
	
	// If all 'null' set relinquishDefault
	if((in1 == null) && (in2 == null) && (in3 == null) && (in4 == null) && (in5 == null) && (in6 == null) && (in7 == null) && (in8 == null) && (in9 == null) && (in10 == null) && (in11 == null) && (in12 == null) && (in13 == null) && (in14 == null) && (in15 == null) && (in16 == null))
    {
	  out := relinquishDefault;
	  analogValuePoint.set(virtualCh, out);
    }
  }
  
  virtual override void changed(Slot slot)
  {
	super.changed(slot);
	
	if (virtualCh != 0)
	{
	  if(slot.name == "out")
	  {
	    analogValuePoint.set(virtualCh, out);
	  }
	  else if((in1 != null) && (enabled==true))
	  {
	    out := in1;
		//analogValuePoint.set(virtualCh, out);
	  }
	  else if((in2 != null) && (enabled==true))
	  {
	    out := in2;
		//analogValuePoint.set(virtualCh, out);
	  }
	  else if((in3 != null) && (enabled==true))
	  {
	    out := in3;
		//analogValuePoint.set(virtualCh, out);
	  }
	  else if((in4 != null) && (enabled==true))
	  {
	    out := in4;
		//analogValuePoint.set(virtualCh, out);
	  }
	  else if((in5 != null) && (enabled==true))
	  {
	    out := in5;
		//analogValuePoint.set(virtualCh, out);
	  }
	  else if((in6 != null) && (enabled==true))
	  {
	    out := in6;
		//analogValuePoint.set(virtualCh, out);
	  }
	  else if((in7 != null) && (enabled==true))
	  {
	    out := in7;
		//analogValuePoint.set(virtualCh, out);
	  }
	  else if((in8 != null) && (enabled==true))
	  {
	    out := in8;
		//analogValuePoint.set(virtualCh, out);
	  }
	  else if((in9 != null) && (enabled==true))
	  {
	    out := in9;
		//analogValuePoint.set(virtualCh, out);
	  }
	  else if((in10 != null) && (enabled==true))
	  {
	    out := in10;
		//analogValuePoint.set(virtualCh, out);
	  }
	  else if((in11 != null) && (enabled==true))
	  {
	    out := in11;
		//analogValuePoint.set(virtualCh, out);
	  }
	  else if((in12 != null) && (enabled==true))
	  {
	    out := in12;
		//analogValuePoint.set(virtualCh, out);
	  }
	  else if((in13 != null) && (enabled==true))
	  {
	    out := in13;
		//analogValuePoint.set(virtualCh, out);
	  }
	  else if((in14 != null) && (enabled==true))
	  {
	    out := in14;
		//analogValuePoint.set(virtualCh, out);
	  }
	  else if((in15 != null) && (enabled==true))
	  {
	    out := in15;
		//analogValuePoint.set(virtualCh, out);
	  }
	  else if((in16 != null) && (enabled==true))
	  {
	    out := in16;
		//analogValuePoint.set(virtualCh, out);
	  }
	  else if (slot.name == "relinquishDefault")
	  {
	    out := relinquishDefault;
	  }
	}
  }
}
