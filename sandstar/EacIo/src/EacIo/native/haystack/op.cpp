//
// Copyright (c) 2015, J2 Innovations
// Copyright (c) 2012 Brian Frank
// Licensed under the Academic Free License version 3.0
// History:
//   08 Sep 2014  Radu Racariu<radur@2inn.com> Ported to C++
//   06 Jun 2011  Brian Frank  Creation
//

#include "op.hpp"
#include "bool.hpp"
#include "datetimerange.hpp"
#include "hisitem.hpp"
#include "logger.h"
#include "marker.hpp"
#include "num.hpp"
#include "points.hpp"
#include "server.hpp"
#include "str.hpp"
#include "uri.hpp"
#include "watch.hpp"
#include "zincreader.hpp"
#include "zincwriter.hpp"
// extern "C" {
//#include "engineio.h"
//}

// std
#include <chrono>
#include <sstream>
#include <stdio.h>
#include <stdlib.h>
#include <thread>
#include <unistd.h>

// poco
#include "Poco/Net/HTMLForm.h"
#include "Poco/Net/HTTPResponse.h"
#include "Poco/Net/NameValueCollection.h"

#include "Poco/DOM/DOMParser.h"
#include "Poco/DOM/Document.h"
#include "Poco/DOM/NamedNodeMap.h"
#include "Poco/DOM/NodeList.h"
#include "Poco/Exception.h"
#include "Poco/Net/HTTPClientSession.h"
#include "Poco/Net/HTTPRequest.h"
#include "Poco/Path.h"
#include "Poco/SAX/InputSource.h"
#include "Poco/URI.h"

// Log
// #include <plog/Log.h>

using namespace haystack;

// Service the request and return response.
// This method routes to "on_service(const Server& db, HTTPServerRequest& req,
// HTTPServerResponse& res)".
void Op::on_service(Server &db, HTTPServerRequest &req, HTTPServerResponse &res) {
  // send response
  res.setStatus(Poco::Net::HTTPResponse::HTTP_OK);
  res.setContentType("text/zinc; charset=utf-8");
  res.set("Access-Control-Allow-Origin", "http://localhost");

  // res.set("Access-Control-Allow-Origin", "http://10.10.20.6");
  // res.set("Access-Control-Allow-Origin", "*");
  // res.set("Access-Control-Allow-Origin", "http://" +
  // req.clientAddress().toString()); res.set("Access-Control-Allow-Origin",
  // "http://" + ((PointServer&)db).m_allowIPs[0]);

  if (std::find(((PointServer &)db).m_allowIPs.begin(), ((PointServer &)db).m_allowIPs.end(), "*") !=
      (((PointServer &)db).m_allowIPs.end())) {
    res.set("Access-Control-Allow-Origin", "*");
  } else {
    try {
      std::string origin = req.get("Origin");

      if (!origin.empty()) {
        // LOG_DEBUG << "ORIGIN: " << origin;
        for (std::vector<std::string>::iterator it = ((PointServer &)db).m_allowIPs.begin(),
                                                end = ((PointServer &)db).m_allowIPs.end();
             it != end; it++) {
          if (it->find(origin) != std::string::npos) {
            res.set("Access-Control-Allow-Origin", *it);
            break;
          }
        }
      }
    } catch (const std::exception &e) {
      // LOG_DEBUG << "Origin: " << e.what();
    }
  }

  res.set("Access-Control-Allow-Method", req.getMethod());

  // LOG_DEBUG << "Access-Control-Allow-Origin: " <<
  // res.get("Access-Control-Allow-Origin"); LOG_DEBUG <<
  // "Access-Control-Allow-Method: " << res.get("Access-Control-Allow-Method");

  std::ostream &ostr = res.send();
  ZincWriter w(ostr);

  try {
    // parse GET query parameters or POST body into grid
    Grid::auto_ptr_t reqGrid;
    const std::string &method = req.getMethod();
    if (method == "GET") {
      reqGrid = get_to_grid(req);
    } else if (method == "POST") {
      reqGrid = post_to_grid(req, res);
    } else {
      return;
    }
    // route to on_service(Server& db, const Grid& req)
    Grid::auto_ptr_t g;

    if (reqGrid.get() != NULL) {
      LOG_DEBUG_HAYSTACKOPS_MSG("request=%s", ZincWriter::grid_to_string(*reqGrid).c_str());
      g = on_service(db, *reqGrid);
    } else {
      g = on_service(db, Grid::EMPTY);
    }

    if (g.get() != NULL) {
      LOG_DEBUG_HAYSTACKOPS_MSG("response='%s'", ZincWriter::grid_to_string(*g).c_str());
      w.write_grid(*g);
    } else {
      LOG_DEBUG_HAYSTACKOPS_MSG("empty response");
      w.write_grid(Grid::EMPTY);
    }
  } catch (std::runtime_error &e) {
    LOG_ERR_HAYSTACKOPS_MSG("request failed");
    w.write_grid(*Grid::make_err(e));
  }
}

// Service the request and return response.
Grid::auto_ptr_t Op::on_service(Server &db, const Grid &req) {
  throw std::runtime_error("Unimplemented op.");
  return Grid::auto_ptr_t();
}

Op::refs_t Op::grid_to_ids(const Server &db, const Grid &grid) const {
  refs_t ids(grid.num_rows());

  for (Grid::const_iterator it = grid.begin(), e = grid.end(); it != e; ++it) {
    const Ref &val = it->get_ref("id");
    ids.push_back((Ref *)val_to_id(db, val).release());
  }
  return ids;
}

Val::auto_ptr_t Op::val_to_id(const Server &db, const Val &val) const {
  if (val.type() == Val::URI_TYPE) {
    Dict::auto_ptr_t rec = db.nav_read_by_uri((Uri &)val, false);
    return rec.get() == NULL ? Ref("").clone() : rec->id().clone();
  } else {
    return val.clone();
  }
}

// Map the GET query parameters to grid with one row
Grid::auto_ptr_t Op::get_to_grid(HTTPServerRequest &req) {
  using namespace Poco::Net;
  HTMLForm form(req);

  // if (form.empty())
  // return Grid::auto_ptr_t();

  NameValueCollection::ConstIterator it = form.begin();
  NameValueCollection::ConstIterator end = form.end();

  Dict d;

  for (; it != end; ++it) {
    const std::string &name = it->first;
    const std::string &val_str = it->second;
    Val::auto_ptr_t val;
    try {
      val = ZincReader::make(val_str)->read_scalar();
    } catch (std::exception &) {
      val = Str(val_str).clone();
    }

    d.add(name, val);
  }
  const std::string path = Poco::URI(req.getURI()).getPath();
  d.add("path", Str(path).clone());
  return Grid::make(d);
}

// Map the POST body to grid
Grid::auto_ptr_t Op::post_to_grid(HTTPServerRequest &req, HTTPServerResponse &res) {
  const std::string &mime = req.getContentType();

  if (mime.find("text/zinc") == mime.npos && mime.find("text/plain") == mime.npos) {
    res.setStatusAndReason(Poco::Net::HTTPResponse::HTTP_NOT_ACCEPTABLE, mime);
    res.send();
    return Grid::auto_ptr_t();
  }
  return ZincReader(req.stream()).read_grid();
}

//////////////////////////////////////////////////////////////////////////
// AboutOp
//////////////////////////////////////////////////////////////////////////
class AboutOp : public Op {
public:
  AboutOp() {}
  const std::string name() const { return "about"; }
  const std::string summary() const { return "Summary information for server"; }

  Grid::auto_ptr_t on_service(Server &db, const Grid &req) { return Grid::make(*db.about()); }
};

//////////////////////////////////////////////////////////////////////////
// OpsOp
//////////////////////////////////////////////////////////////////////////
class OpsOp : public Op {
public:
  const std::string name() const { return "ops"; }
  const std::string summary() const { return "Operations supported by this server"; }

  Grid::auto_ptr_t on_service(Server &db, const Grid &req) {
    if (ops_grid.get() != NULL) {
      // construct a static GridView
      static GridView gv(*ops_grid);
      // we return copies of a GridView, this should be fairly light
      return Grid::auto_ptr_t(new GridView(gv));
    }

    Grid::auto_ptr_t g(new Grid);

    g->add_col("name");
    g->add_col("summary");

    StdOps::ops_map_t::const_iterator it = StdOps::ops_map().begin();
    StdOps::ops_map_t::const_iterator end = StdOps::ops_map().end();

    for (; it != end; ++it) {
      Val *vals[2] = {new Str(it->second->name()), new Str(it->second->summary())};
      g->add_row(vals, sizeof(vals) / sizeof(Val *));
    }

    if (ops_grid.get() == NULL)
      ops_grid = g;

    return Grid::auto_ptr_t(new GridView(*ops_grid));
  }

private:
  Grid::auto_ptr_t ops_grid;
};

//////////////////////////////////////////////////////////////////////////
// FormatsOp
//////////////////////////////////////////////////////////////////////////
class FormatsOp : public Op {
public:
  FormatsOp() {}
  const std::string name() const { return "formats"; }
  const std::string summary() const { return "Grid data formats supported by this server"; }

  Grid::auto_ptr_t on_service(Server &db, const Grid &req) {
    // construct a static GridView
    static GridView gv(*fmt_grid);
    // we return copies of a GridView, this should be fairly light
    return Grid::auto_ptr_t(new GridView(gv));
  }

private:
  static const Grid::auto_ptr_t fmt_grid;
  static Grid::auto_ptr_t fmt_grid_init();
};

Grid::auto_ptr_t FormatsOp::fmt_grid_init() {
  Grid::auto_ptr_t g(new Grid);

  // init the response grid
  g->add_col("mime");
  g->add_col("read");
  g->add_col("write");

  Val *v[3] = {new Str("text/zinc"), new Marker(), new Marker()};
  g->add_row(v, 3);

  return g;
}
const Grid::auto_ptr_t FormatsOp::fmt_grid = fmt_grid_init();

//////////////////////////////////////////////////////////////////////////
// ReadOp
//////////////////////////////////////////////////////////////////////////
class ReadOp : public Op {
public:
  ReadOp() {}
  const std::string name() const { return "read"; }
  const std::string summary() const { return "Read entity records in database"; }

  Grid::auto_ptr_t on_service(Server &db, const Grid &req) {
    // ensure we have one row
    if (req.is_empty())
      throw std::runtime_error("Request has no rows");

    // perform filter or id read
    const Row &row = req.row(0);

    if (row.has("filter")) {
      // filter read
      const std::string &filter = row.get_string("filter");
      size_t limit = static_cast<size_t>(row.has("limit") ? row.get_double("limit") : (size_t)-1);
      return db.read_all(filter, limit);
    } else if (row.has("id")) {
      boost::ptr_vector<Ref> v;

      Grid::const_iterator it = req.begin();
      for (; it != req.end(); ++it) {
        if (it->has("id"))
          v.push_back(new_clone((Ref &)it->get("id")));
      }

      return db.read_by_ids(v);
    }

    return Grid::auto_ptr_t();
  }
};

//////////////////////////////////////////////////////////////////////////
// NavOp
//////////////////////////////////////////////////////////////////////////
class NavOp : public Op {
public:
  NavOp() {}
  const std::string name() const { return "nav"; }
  const std::string summary() const { return "Navigate record tree"; }

  Grid::auto_ptr_t on_service(Server &db, const Grid &req) {
    // ensure we have one row
    // LOG_INFO_HAYSTACKOPS_MSG("in NavOp::on_service");
    // for xxx.xxx.xxx.xxx:yyyy/nav
    if (req.is_empty()) {
      LOG_INFO_HAYSTACKOPS_MSG("req.is_empty");
      return db.nav("");
    } else {
      // for xxx.xxx.xxx.xxx:yyyy/nav?navId=xxxxxxxxxxxxxxxxxxxx
      const Val &nav_id_val = req.row(0).get("navId", false);
      if (nav_id_val.type() == Val::STR_TYPE) {
        const std::string nav_id = nav_id_val.as<Str>().value;
        // LOG_INFO_HAYSTACKOPS_MSG("req navId str");
        return db.nav(nav_id);
      }
      // for xxx.xxx.xxx.xxx:yyyy/nav?disType=component
      const Val &dis_type_val = req.row(0).get("disType", false);
      if (dis_type_val.type() == Val::STR_TYPE && dis_type_val.as<Str>().value.compare("component") == 0) {
        return handleComponentListRequest();
      }
    }
    return db.nav("");
  }

private:
  Grid::auto_ptr_t handleComponentListRequest() {
    Grid::auto_ptr_t res(new Grid);
    // res->reserve_rows(3);
    res->add_col("name");
    res->add_col("id");
    res->add_col("type");
    Poco::URI uri1("http://127.0.0.1:8080/spy/backup");
    std::string path1(uri1.getPathAndQuery());
    if (path1.empty())
      path1 = "/";
    Poco::Net::HTTPClientSession session1(uri1.getHost(), uri1.getPort());
    Poco::Net::HTTPRequest request1(Poco::Net::HTTPRequest::HTTP_GET, path1, Poco::Net::HTTPMessage::HTTP_1_1);
    try {
      session1.sendRequest(request1);
    } catch (Poco::Exception &exc) {
      LOG_ERR_HAYSTACKOPS_MSG("%s", exc.displayText().c_str());
    }
    Poco::Net::HTTPResponse response1;
    std::istream &rs1 = session1.receiveResponse(response1);
    if (response1.getStatus() == 200) {
      Poco::XML::InputSource src(rs1);
      try {
        Poco::XML::DOMParser parser;
        auto pDoc = parser.parse(&src);
        auto *list = pDoc->getElementsByTagNameNS("", "sedonaApp");
        if (list->length() == 1) {
          Poco::XML::Node *sedonaAppNode = list->item(0);
          auto sedonaList = sedonaAppNode->childNodes();
          if (sedonaList->length() > 0) {
            bool bAppFound = false;
            for (int sedonaChildIndex = 0; sedonaChildIndex < sedonaList->length(); ++sedonaChildIndex) {
              if (sedonaList->item(sedonaChildIndex)->nodeName().compare("app") == 0) {
                bAppFound = true;
                auto appList = sedonaList->item(sedonaChildIndex)->childNodes();
                for (int appChildIndex = 0; appChildIndex < appList->length(); ++appChildIndex) {
                  if (appList->item(appChildIndex)->nodeName().compare("comp") == 0) {
                    auto attributes = appList->item(appChildIndex)->attributes();
                    if (attributes != nullptr) {
                      auto tNode = attributes->getNamedItem("name");
                      if (tNode->nodeValue().compare("service") != 0) {
                        std::unique_ptr<Val *[]> rowValue((new Val *[res->num_cols()]));
                        rowValue[0] = new_clone(Str(attributes->getNamedItem("name")->nodeValue()));
                        rowValue[1] = new_clone(Str(attributes->getNamedItem("id")->nodeValue()));
                        rowValue[2] = new_clone(Str(attributes->getNamedItem("type")->nodeValue()));
                        res->add_row(rowValue.get(), res->num_cols());
                      }
                      attributes->release();
                    }
                  }
                }
                break;
              }
            }
            if (bAppFound == false) {
              LOG_ERR_HAYSTACKOPS_MSG("Invalid XML,No child 'app' in 'sedonaApp'");
            }
          } else {
            LOG_ERR_HAYSTACKOPS_MSG("Invalid XML,No child in 'sedonaApp'");
          }
        } else {
          LOG_ERR_HAYSTACKOPS_MSG("Invalid XML,No or more than one 'sedonaApp' in xml responce");
        }
      } catch (Poco::Exception &exc) {
        LOG_ERR_HAYSTACKOPS_MSG("%s", exc.displayText().c_str());
      }
    } else {
      LOG_ERR_HAYSTACKOPS_MSG("SOX http request failed");
    }
    return res;
  }
};

//////////////////////////////////////////////////////////////////////////
// WatchSubOp
//////////////////////////////////////////////////////////////////////////
class WatchSubOp : public Op {
public:
  WatchSubOp() {}
  const std::string name() const { return "watchSub"; }
  const std::string summary() const { return "Watch subscription"; }

  Grid::auto_ptr_t on_service(Server &db, const Grid &req) {
    // check for watchId or watchId
    std::string watchId;
    std::string watchDis;

    if (req.meta().has("watchId"))
      watchId = req.meta().get_str("watchId");
    else
      watchDis = req.meta().get_str("watchDis");

    // open or lookup watch
    Watch::shared_ptr watch = watchId.empty() ? db.watch_open(watchDis) : db.watch(watchId);

    if (watch.get() == NULL)
      return Grid::make_err(std::runtime_error("Watch not found."));

    // map grid to ids
    const Op::refs_t &ids = grid_to_ids(db, req);

    // subscribe and return resulting grid
    return watch->sub(ids);
  }
};

//////////////////////////////////////////////////////////////////////////
// WatchUnsubOp
//////////////////////////////////////////////////////////////////////////
class WatchUnsubOp : public Op {
public:
  WatchUnsubOp() {}
  const std::string name() const { return "watchUnsub"; }
  const std::string summary() const { return "Watch unsubscription"; }

  Grid::auto_ptr_t on_service(Server &db, const Grid &req) {
    // lookup watch, silently ignore failure
    const std::string &watchId = req.meta().get_str("watchId");
    Watch::shared_ptr watch = db.watch(watchId, false);

    // check for close or unsub
    if (watch.get() != NULL) {
      if (req.meta().has("close"))
        watch->close();
      else
        watch->unsub(grid_to_ids(db, req));
    }

    // nothing to return
    return Grid::auto_ptr_t();
  }
};

//////////////////////////////////////////////////////////////////////////
// WatchPollOp
//////////////////////////////////////////////////////////////////////////
class WatchPollOp : public Op {
public:
  WatchPollOp() {}
  const std::string name() const { return "watchPoll"; }
  const std::string summary() const { return "Watch poll cov or refresh"; }

  Grid::auto_ptr_t on_service(Server &db, const Grid &req) {
    // lookup watch
    const std::string &watchId = req.meta().get_str("watchId");
    Watch::shared_ptr watch = db.watch(watchId);

    // check for refresh or changes
    if (watch.get() != NULL) {
      // poll cov or refresh
      if (req.meta().has("refresh"))
        return watch->poll_refresh();
      else
        return watch->poll_changes();
    }
    Grid::auto_ptr_t g = Grid::make_err(std::runtime_error("Watch not found."));
    g->meta().add("watchId", watchId);
    return g;
  }
};

//////////////////////////////////////////////////////////////////////////
// List all watches op
//////////////////////////////////////////////////////////////////////////
class WatchListOp : public Op {
public:
  WatchListOp() {}
  const std::string name() const { return "watchList"; }
  const std::string summary() const { return "List all watches registered"; }

  Grid::auto_ptr_t on_service(Server &db, const Grid &req) {
    boost::ptr_vector<Dict> rows;
    const std::vector<Watch::shared_ptr> &ws = db.watches();
    for (std::vector<Watch::shared_ptr>::const_iterator it = ws.begin(), end = ws.end(); it != end; ++it) {
      Dict::auto_ptr_t d(new Dict);
      d->add("id", (*it)->id());
      d->add("dis", (*it)->dis());
      d->add("lease", (*it)->lease(), "sec");
      d->add("isOpen", Bool((*it)->is_open()));
      rows.push_back(d);
    }

    if (rows.empty()) {
      Grid::auto_ptr_t g = Grid::make_err(std::runtime_error("Watch list empty."));
      return g;
    }
    return Grid::make(rows);
  }
};

//////////////////////////////////////////////////////////////////////////
// PointWriteOp
//////////////////////////////////////////////////////////////////////////
class PointWriteOp : public Op {
public:
  PointWriteOp() {}
  const std::string name() const { return "pointWrite"; }
  const std::string summary() const { return "Read/write writable point priority array"; }

  Grid::auto_ptr_t on_service(Server &db, const Grid &req) {
    // get required point id
    if (req.is_empty())
      throw std::runtime_error("Request has no rows");
    const Row &row = req.row(0);
    Val::auto_ptr_t id = val_to_id(db, row.get("id"));

    // check for write
    if (row.has("level")) {
      int level = static_cast<int>(row.get_int("level"));
      const std::string &who = row.get_str("who"); // be nice to have user fallback
      const Val &val = row.get("val", false);
      const Num &dur = (Num &)row.get("duration", false);
      db.point_write(id->as<Ref>(), level, val, who, dur);
    }

    return db.point_write_array(id->as<Ref>());
  }
};

//////////////////////////////////////////////////////////////////////////
// HisReadOp
//////////////////////////////////////////////////////////////////////////
class HisReadOp : public Op {
public:
  HisReadOp() {}
  const std::string name() const { return "hisRead"; }
  const std::string summary() const { return "Read time series from historian"; }

  Grid::auto_ptr_t on_service(Server &db, const Grid &req) {
    if (req.is_empty())
      throw std::runtime_error("Request has no rows");
    const Row &row = req.row(0);
    Val::auto_ptr_t id = val_to_id(db, row.get("id"));

    const std::string &r = row.get_str("range");
    return db.his_read((Ref &)*id, r);
  }
};

//////////////////////////////////////////////////////////////////////////
// HisWriteOp
//////////////////////////////////////////////////////////////////////////
class HisWriteOp : public Op {
public:
  HisWriteOp() {}
  const std::string name() const { return "hisWrite"; }
  const std::string summary() const { return "Write time series data to historian"; }

  Grid::auto_ptr_t on_service(Server &db, const Grid &req) {
    if (req.is_empty())
      throw std::runtime_error("Request has no rows");
    Val::auto_ptr_t id = val_to_id(db, req.meta().get("id"));

    std::vector<HisItem> items = HisItem::grid_to_items(req);
    db.his_write(id->as<Ref>(), items);

    return Grid::auto_ptr_t();
  }
};

//////////////////////////////////////////////////////////////////////////
// InvokeActionOp
//////////////////////////////////////////////////////////////////////////
class InvokeActionOp : public Op {
public:
  InvokeActionOp() {}
  const std::string name() const { return "invokeAction"; }
  const std::string summary() const { return "Invoke action on target entity"; }

  Grid::auto_ptr_t on_service(Server &db, const Grid &req) {
    Val::auto_ptr_t id = val_to_id(db, req.meta().get("id"));

    const std::string &action = req.meta().get_str("action");

    if (req.num_rows() > 0)
      return db.invoke_action(id->as<Ref>(), action, req.row(0));
    else
      return db.invoke_action(id->as<Ref>(), action, Dict::EMPTY);
  }
};

//////////////////////////////////////////////////////////////////////////
// commit ops
//////////////////////////////////////////////////////////////////////////
void updatePoint(const Grid &g);
void addPoint(const Grid &g);
void deletePoint(const Grid &g);
t_ErrOp optimizeGrid();
void commitZinc();

const char UPDATE[] = "update";
const char DELETE[] = "delete";
const char ADD[] = "add";
const char OPTIMIZE[] = "optimize";
const char ZINC[] = "zinc";

class CommitOp : public Op {
public:
  CommitOp() {}
  const std::string name() const { return "commit"; }
  const std::string summary() const {
    return "Commit changes. 'add': Add point, 'delete': Delete point, "
           "'update': update point, 'optimize': optimize grid, 'zinc': write "
           "current updated zinc database to file";
  }

  Grid::auto_ptr_t on_service(Server &db, const Grid &req) {
    // ensure we have one row
    if (req.is_empty())
      throw std::runtime_error("Request has no rows");

    // LOG_DEBUG << ZincWriter::grid_to_string(req);

    const Val &val = req.meta().get("commit");

    Dict d;
    d.add("op", Str("commit").clone());
    d.add("subOp", val.clone());
    d.add("status", Str("fail").clone());

    // TODO: Set return values of these ops to dicts

    if (val == Str(UPDATE)) {
      // LOG_DEBUG << "UPDATE";
      updatePoint(req);
      d.update("status", "ok");
    } else if (val == Str(ADD)) {
      // LOG_DEBUG << "ADD";
      addPoint(req);
      d.update("status", "ok");
    } else if (val == Str(DELETE)) {
      // LOG_DEBUG << "DELETE";
      deletePoint(req);
      d.update("status", "ok");
    } else if (val == Str(OPTIMIZE)) {
      t_ErrOp err;
      // LOG_DEBUG << "OPTIMIZE";
      err = optimizeGrid();
      d.update("status", err);
    } else if (val == Str(ZINC)) {
      commitZinc();
      d.update("status", "ok");
    } else {
      // LOG_DEBUG << "ERROR";
      throw std::runtime_error("Invalid commit operation. Allowed commit operation is 'add', "
                               "'update', 'delete', 'optimize', 'zinc'");
    }

    return Grid::make(d);
    // return  Grid::auto_ptr_t();
  }
};

//////////////////////////////////////////////////////////////////////////
// restart sandstar ops
//////////////////////////////////////////////////////////////////////////
class RestartOp : public Op {
public:
  RestartOp() {}
  const std::string name() const { return "restart"; }
  const std::string summary() const { return "Restart Sandstar"; }

  Grid::auto_ptr_t on_service(Server &db, const Grid &req) {
    int err = -1;
    // ensure we have one row
    // if (req.is_empty())
    //    throw std::runtime_error("Request has no rows");

    Dict d;
    d.add("status", Str("restartFailed").clone());

    // char *const parmList[] = {"/bin/systemctl", "start",
    // "sandstar_engine.service", NULL}; execv("/bin/systemctl", parmList);

    //*** TODO ***
    // This is not the good method to restart service. Find better solution
    //************

    // This will send signal to systemctl script. In sandstar.service, on
    // receiving exit signal, it will reboot service
    exit(0);

    return Grid::make(d);
    // return  Grid::auto_ptr_t();
  }
};

//////////////////////////////////////////////////////////////////////////
// restart sandstar ops
//////////////////////////////////////////////////////////////////////////
class RootOp : public Op {
public:
  RootOp() {}
  const std::string name() const { return "root"; }
  const std::string summary() const { return "Root Sandstar"; }

  Grid::auto_ptr_t on_service(Server &db, const Grid &req) {
    if (req.num_cols() > 0) {
      const Col *col = req.col("path", false);
      if (col != nullptr) {
        const Row &row = req.row(0);
        std::string strPath = row.get_string("path");
        size_t slashPos = strPath.find_first_of('/', 1);
        if (slashPos != std::string::npos) {
          std::string strLatter = strPath.substr(slashPos);
          if (strLatter.size() > 1) {
            slashPos = strLatter.find_first_of('/', 1);
            if (slashPos != std::string::npos) {
              slashPos--;
            }
            std::string strId = strLatter.substr(1, slashPos);
            // return handleComponent(std::stoi(strId));
            return handleComponent(strLatter);
          }
        }
      }
    }
    return handleComponentListRequest();
  }

private:
  Poco::XML::Node *findNode(Poco::XML::Node *pNode, const std::string &strId) {
    Poco::XML::Node *ret = nullptr;
    if (pNode != nullptr) {
      auto pNodeList = pNode->childNodes();
      if (pNodeList != nullptr) {
        for (int index = 0; index < pNodeList->length() && ret == nullptr; ++index) {
          auto pChildNode = pNodeList->item(index);
          if (pChildNode->hasAttributes()) {
            auto pChildAttrs = pChildNode->attributes();
            for (int attrIndex = 0; attrIndex < pChildAttrs->length(); ++attrIndex) {
              if (pChildAttrs->item(attrIndex)->nodeName().compare("id") == 0 &&
                  pChildAttrs->item(attrIndex)->nodeValue().compare(strId) == 0) {
                ret = pChildNode;
                break;
              }
            }
          }
        }
      }
    }
    return ret;
  }
  void handleCompTag(Poco::XML::Node *pChildNode, Grid::auto_ptr_t &res, std::map<std::string, Val *> &mapRow) {
    auto pChildAttrs = pChildNode->attributes();
    for (int attrIndex = 0; attrIndex < pChildAttrs->length(); ++attrIndex) {
      std::cout << __FILE__ << ":" << __LINE__ << std::endl;
      std::cout << pChildAttrs->item(attrIndex)->nodeName() << ":" << pChildAttrs->item(attrIndex)->nodeValue()
                << std::endl;
      if (res->is_column_present(pChildAttrs->item(attrIndex)->nodeName()) == false) {
        res->add_col(pChildAttrs->item(attrIndex)->nodeName());
      }
      if (pChildAttrs->item(attrIndex)->nodeName().compare("id") == 0) {
        mapRow[pChildAttrs->item(attrIndex)->nodeName()] = new_clone(Ref(pChildAttrs->item(attrIndex)->nodeValue()));
      } else {
        mapRow[pChildAttrs->item(attrIndex)->nodeName()] = new_clone(Str(pChildAttrs->item(attrIndex)->nodeValue()));
      }
    }
    pChildAttrs->release();
  }
  void handlePropTag(Poco::XML::Node *pChildNode, Grid::auto_ptr_t &res, std::map<std::string, Val *> &mapRow) {
    auto pChildAttrs = pChildNode->attributes();
    auto pType = pChildAttrs->getNamedItem("type");
    std::cout << "type:" << pType->nodeName() << ":" << pType->nodeValue() << std::endl;
    Val *pVal = nullptr;
    if (pType) {
      if (pType->nodeValue().compare("int") == 0) {
        pVal = new_clone(Num(std::stoi(pChildAttrs->getNamedItem("val")->nodeValue())));
      } else if (pType->nodeValue().compare("Buf") == 0) {
        pVal = new_clone(Str(pChildAttrs->getNamedItem("val")->nodeValue()));
      } else if (pType->nodeValue().compare("long") == 0) {
        pVal = new_clone(Num(std::stoll(pChildAttrs->getNamedItem("val")->nodeValue())));
      } else if (pType->nodeValue().compare("bool") == 0) {
        pVal = new_clone(Bool(pChildAttrs->getNamedItem("val")->nodeValue().compare("true") ? true : false));
      }
    }
    for (int attrIndex = 0; attrIndex < pChildAttrs->length(); ++attrIndex) {
      std::cout << __FILE__ << ":" << __LINE__ << std::endl;
      std::cout << pChildAttrs->item(attrIndex)->nodeName() << ":" << pChildAttrs->item(attrIndex)->nodeValue()
                << std::endl;
      if (res->is_column_present(pChildAttrs->item(attrIndex)->nodeName()) == false) {
        res->add_col(pChildAttrs->item(attrIndex)->nodeName());
      }
      if (pChildAttrs->item(attrIndex)->nodeName().compare("id") == 0) {
        mapRow[pChildAttrs->item(attrIndex)->nodeName()] = new_clone(Ref(pChildAttrs->item(attrIndex)->nodeValue()));
      } else if (pChildAttrs->item(attrIndex)->nodeName().compare("val") != 0) {
        mapRow[pChildAttrs->item(attrIndex)->nodeName()] = new_clone(Str(pChildAttrs->item(attrIndex)->nodeValue()));
      }
    }
    if (pVal) {
      mapRow["val"] = pVal;
    }
    pChildAttrs->release();
  }
  Grid::auto_ptr_t handleComponent(const std::string &strLatter) {
    Grid::auto_ptr_t res(new Grid);
    Poco::XML::Document *pDoc = nullptr;
    if (getSpyBackUp(&pDoc)) {
      auto pSedonaAppNode = getSedonaAppNode(pDoc);
      if (pSedonaAppNode != nullptr) {
        auto pAppNode = getChildNode(pSedonaAppNode, "app");
        if (pAppNode != nullptr) {
          Poco::XML::Node *pNode = pAppNode;
          size_t startPos = 1;
          size_t slashPos = 0;
          std::string strId;
          for (; startPos != std::string::npos && slashPos < strLatter.size() - 1;) {
            slashPos = strLatter.find_first_of('/', startPos);
            strId = strLatter.substr(startPos, slashPos - startPos);
            if (strId.empty() == false) {
              pNode = findNode(pNode, strId);
              if (pNode == nullptr) {
                std::cout << "Node not found" << std::endl;
                break;
              }
              if (slashPos != std::string::npos)
                startPos = slashPos + 1;
              else
                startPos = std::string::npos;
            }
          }
          if (pNode != nullptr && pNode != pAppNode) {
            auto pChildList = pNode->childNodes();
            std::vector<std::map<std::string, Val *>> vecRow;
            for (int index = 0; index < pChildList->length(); ++index) {
              auto pChildNode = pChildList->item(index);
              if (pChildNode->hasAttributes()) {
                std::map<std::string, Val *> mapRow;
                std::cout << pChildNode->nodeName() << std::endl;
                if (res->is_column_present("nodeType") == false) {
                  res->add_col("nodeType");
                }
                mapRow["nodeType"] = new_clone(Str(pChildNode->nodeName()));
                // res->add_col("nodeType");
                // auto pChildAttrs = pChildNode->attributes();
                if (pChildNode->nodeName().compare("comp") == 0) {
                  handleCompTag(pChildNode, res, mapRow);
                } else if (pChildNode->nodeName().compare("prop") == 0) {
                  handlePropTag(pChildNode, res, mapRow);
                } else {
                  auto pChildAttrs = pChildNode->attributes();
                  for (int attrIndex = 0; attrIndex < pChildAttrs->length(); ++attrIndex) {
                    if (res->is_column_present(pChildAttrs->item(attrIndex)->nodeName()) == false) {
                      res->add_col(pChildAttrs->item(attrIndex)->nodeName());
                    }
                    mapRow[pChildAttrs->item(attrIndex)->nodeName()] =
                        new_clone(Str(pChildAttrs->item(attrIndex)->nodeValue()));
                  }
                  pChildAttrs->release();
                }
                // pChildAttrs->release();
                if (mapRow.size() > 0)
                  vecRow.push_back(mapRow);
              } // if (pChildNode->hasAttributes())
            }
            for (auto &row : vecRow) {
              res->add_row(row);
            }
          } else {
            throw std::runtime_error("Invalid component id");
          }
        } else { // if (pAppNode != nullptr)
          LOG_ERR_HAYSTACKOPS_MSG("No 'app' node found in document");
        }      // if (pAppNode != nullptr)
      } else { // if (pSedonaAppNode != nullptr)
        LOG_ERR_HAYSTACKOPS_MSG("No 'sedonaApp' node found in document");
      } // if (pSedonaAppNode != nullptr)
    }   // if (getSpyBackUp(&pDoc))
    return res;
  }
  Grid::auto_ptr_t handleComponent(int id) {
    Grid::auto_ptr_t res(new Grid);
    const std::string strId = std::to_string(id);
    Poco::XML::Document *pDoc = nullptr;
    if (getSpyBackUp(&pDoc)) {
      auto pSedonaAppNode = getSedonaAppNode(pDoc);
      if (pSedonaAppNode != nullptr) {
        auto pAppNode = getChildNode(pSedonaAppNode, "app");
        if (pAppNode != nullptr) {
          auto pCompNode = getChildNode(pAppNode, "comp");
          while (pCompNode != nullptr) {
            if (pCompNode->nodeName().compare("comp") == 0) {
              auto attributes = pCompNode->attributes();
              if (strId.compare(attributes->getNamedItem("id")->nodeValue()) == 0) {
                auto pChildList = pCompNode->childNodes();
                std::vector<std::map<std::string, Val *>> vecRow;
                res->add_col("nodeType");
                for (int index = 0; index < pChildList->length(); ++index) {
                  auto pChildNode = pChildList->item(index);
                  if (pChildNode->hasAttributes()) {
                    std::map<std::string, Val *> mapRow;
                    mapRow["nodeType"] = new_clone(Str(pChildNode->nodeName()));
                    auto pChildAttrs = pChildNode->attributes();
                    for (int attrIndex = 0; attrIndex < pChildAttrs->length(); ++attrIndex) {
                      if (res->is_column_present(pChildAttrs->item(attrIndex)->nodeName()) == false) {
                        res->add_col(pChildAttrs->item(attrIndex)->nodeName());
                      }
                      if (pChildAttrs->item(attrIndex)->nodeName().compare("id") == 0) {
                        mapRow[pChildAttrs->item(attrIndex)->nodeName()] =
                            new_clone(Ref(pChildAttrs->item(attrIndex)->nodeValue()));
                      } else {
                        mapRow[pChildAttrs->item(attrIndex)->nodeName()] =
                            new_clone(Str(pChildAttrs->item(attrIndex)->nodeValue()));
                      }
                    }
                    pChildAttrs->release();
                    vecRow.push_back(mapRow);
                  } // if (pChildNode->hasAttributes())
                }   // for (int index = 0; index < pChildList->length(); ++index)
                for (auto &row : vecRow) {
                  res->add_row(row);
                }
                break;
              } // if (strId.compare(attributes->getNamedItem("id")->nodeValue()) == 0)
              attributes->release();
            } // if (pCompNode->nodeName().compare("comp") == 0)
            pCompNode = pCompNode->nextSibling();
          }      // while (pCompNode != nullptr)
        } else { // if (pAppNode != nullptr)
          LOG_ERR_HAYSTACKOPS_MSG("No 'app' node found in document");
        }      // if (pAppNode != nullptr)
      } else { // if (pSedonaAppNode != nullptr)
        LOG_ERR_HAYSTACKOPS_MSG("No 'sedonaApp' node found in document");
      } // if (pSedonaAppNode != nullptr)
    }   // if (getSpyBackUp(&pDoc))
    return res;
  }
  bool getSpyBackUp(Poco::XML::Document **xml) {
    bool ret = false;
    Poco::URI uri1("http://127.0.0.1:8080/spy/backup");
    std::string path1(uri1.getPathAndQuery());
    if (path1.empty())
      path1 = "/";
    Poco::Net::HTTPClientSession session1(uri1.getHost(), uri1.getPort());
    Poco::Net::HTTPRequest request1(Poco::Net::HTTPRequest::HTTP_GET, path1, Poco::Net::HTTPMessage::HTTP_1_1);
    try {
      session1.sendRequest(request1);
      Poco::Net::HTTPResponse response1;
      std::istream &rs1 = session1.receiveResponse(response1);
      if (response1.getStatus() == 200) {
        Poco::XML::InputSource src(rs1);
        Poco::XML::DOMParser parser;
        *xml = parser.parse(&src);
        ret = true;
      }
    } catch (Poco::Exception &exc) {
      LOG_ERR_HAYSTACKOPS_MSG("%s", exc.displayText().c_str());
    }
    return ret;
  }
  Poco::XML::Node *getSedonaAppNode(Poco::XML::Document *pDoc) {
    Poco::XML::Node *ret = nullptr;
    if (pDoc != nullptr) {
      auto *list = pDoc->getElementsByTagNameNS("", "sedonaApp");
      if (list->length() == 1) {
        ret = list->item(0);
      } else {
        LOG_ERR_HAYSTACKOPS_MSG("Invalid XML,No or more than one 'sedonaApp' in xml responce");
      }
    } else {
      LOG_ERR_HAYSTACKOPS_MSG("Input document is null");
    }
    return ret;
  }
  Poco::XML::Node *getChildNode(Poco::XML::Node *pNode, const std::string &name) {
    Poco::XML::Node *ret = nullptr;
    if (pNode != nullptr) {
      auto childList = pNode->childNodes();
      for (int index = 0; index < childList->length(); ++index) {
        if (childList->item(index)->nodeName().compare(name) == 0) {
          ret = childList->item(index);
          break;
        }
      }
    } else {
      LOG_ERR_HAYSTACKOPS_MSG("Input Node is null");
    }
    return ret;
  }
  Grid::auto_ptr_t handleComponentListRequest() {
    Grid::auto_ptr_t res(new Grid);
    res->add_col("name");
    res->add_col("id");
    res->add_col("type");
    Poco::XML::Document *pDoc = nullptr;
    if (getSpyBackUp(&pDoc)) {
      auto pSedonaAppNode = getSedonaAppNode(pDoc);
      if (pSedonaAppNode != nullptr) {
        auto pAppNode = getChildNode(pSedonaAppNode, "app");
        if (pAppNode != nullptr) {
          auto pCompNode = getChildNode(pAppNode, "comp");
          while (pCompNode != nullptr) {
            if (pCompNode->nodeName().compare("comp") == 0) {
              auto attributes = pCompNode->attributes();
              std::unique_ptr<Val *[]> rowValue((new Val *[res->num_cols()]));
              rowValue[0] = new_clone(Str(attributes->getNamedItem("name")->nodeValue()));
              rowValue[1] = new_clone(Ref(attributes->getNamedItem("id")->nodeValue()));
              rowValue[2] = new_clone(Str(attributes->getNamedItem("type")->nodeValue()));
              res->add_row(rowValue.get(), res->num_cols());
              attributes->release();
            }
            pCompNode = pCompNode->nextSibling();
          }
        } else {
          LOG_ERR_HAYSTACKOPS_MSG("No 'app' node found in document");
        }
      } else {
        LOG_ERR_HAYSTACKOPS_MSG("No 'sedonaApp' node found in document");
      }
    }
    return res;
  }

  Grid::auto_ptr_t handleComponentListRequest1() {
    Grid::auto_ptr_t res(new Grid);
    // res->reserve_rows(3);
    res->add_col("name");
    res->add_col("id");
    res->add_col("type");
    Poco::URI uri1("http://127.0.0.1:8080/spy/backup");
    std::string path1(uri1.getPathAndQuery());
    if (path1.empty())
      path1 = "/";
    Poco::Net::HTTPClientSession session1(uri1.getHost(), uri1.getPort());
    Poco::Net::HTTPRequest request1(Poco::Net::HTTPRequest::HTTP_GET, path1, Poco::Net::HTTPMessage::HTTP_1_1);
    try {
      session1.sendRequest(request1);
    } catch (Poco::Exception &exc) {
      LOG_ERR_HAYSTACKOPS_MSG("%s", exc.displayText().c_str());
    }
    Poco::Net::HTTPResponse response1;
    std::istream &rs1 = session1.receiveResponse(response1);
    if (response1.getStatus() == 200) {
      Poco::XML::InputSource src(rs1);
      try {
        Poco::XML::DOMParser parser;
        auto pDoc = parser.parse(&src);
        auto *list = pDoc->getElementsByTagNameNS("", "sedonaApp");
        if (list->length() == 1) {
          Poco::XML::Node *sedonaAppNode = list->item(0);
          auto sedonaList = sedonaAppNode->childNodes();
          if (sedonaList->length() > 0) {
            bool bAppFound = false;
            for (int sedonaChildIndex = 0; sedonaChildIndex < sedonaList->length(); ++sedonaChildIndex) {
              if (sedonaList->item(sedonaChildIndex)->nodeName().compare("app") == 0) {
                bAppFound = true;
                auto appList = sedonaList->item(sedonaChildIndex)->childNodes();
                for (int appChildIndex = 0; appChildIndex < appList->length(); ++appChildIndex) {
                  if (appList->item(appChildIndex)->nodeName().compare("comp") == 0) {
                    auto attributes = appList->item(appChildIndex)->attributes();
                    if (attributes != nullptr) {
                      auto tNode = attributes->getNamedItem("name");
                      if (tNode->nodeValue().compare("service") != 0) {
                        std::unique_ptr<Val *[]> rowValue((new Val *[res->num_cols()]));
                        rowValue[0] = new_clone(Str(attributes->getNamedItem("name")->nodeValue()));
                        rowValue[1] = new_clone(Str(attributes->getNamedItem("id")->nodeValue()));
                        rowValue[2] = new_clone(Str(attributes->getNamedItem("type")->nodeValue()));
                        res->add_row(rowValue.get(), res->num_cols());
                      }
                      attributes->release();
                    }
                  }
                }
                break;
              }
            }
            if (bAppFound == false) {
              LOG_ERR_HAYSTACKOPS_MSG("Invalid XML,No child 'app' in 'sedonaApp'");
            }
          } else {
            LOG_ERR_HAYSTACKOPS_MSG("Invalid XML,No child in 'sedonaApp'");
          }
        } else {
          LOG_ERR_HAYSTACKOPS_MSG("Invalid XML,No or more than one 'sedonaApp' in xml responce");
        }
      } catch (Poco::Exception &exc) {
        LOG_ERR_HAYSTACKOPS_MSG("%s", exc.displayText().c_str());
      }
    } else {
      LOG_ERR_HAYSTACKOPS_MSG("SOX http request failed");
    }
    return res;
  }
};

// List the registered operations.
const Op &StdOps::about = AboutOp();
// List the registered grid formats.
const Op &StdOps::formats = FormatsOp();
// Read entity records in database.
const Op &StdOps::read = ReadOp();
// Navigate tree structure of database.
const Op &StdOps::nav = NavOp();
// Watch subscription.
const Op &StdOps::watch_sub = WatchSubOp();
// Watch unsubscription.
const Op &StdOps::watch_unsub = WatchUnsubOp();
// Watch poll cov or refresh.
const Op &StdOps::watch_poll = WatchPollOp();
// List all Watches.
const Op &StdOps::watch_list = WatchListOp();
// Read/write writable point priority array.
const Op &StdOps::point_write = PointWriteOp();
// Read time series history data.
const Op &StdOps::his_read = HisReadOp();
// Write time series history data.
const Op &StdOps::his_write = HisWriteOp();
// Invoke action.
const Op &StdOps::invoke_action = InvokeActionOp();
// Invoke commit
const Op &StdOps::commit = CommitOp();
// Invoke restart
const Op &StdOps::restart = RestartOp();
// root op
const Op &StdOps::root = RootOp();

// List the registered operations.
const Op &StdOps::ops = *new OpsOp();

const StdOps::ops_map_t &StdOps::ops_map() {
  if (m_ops_map != NULL)
    return *m_ops_map;

  StdOps::m_ops_map = new StdOps::ops_map_t();

  m_ops_map->insert(std::pair<std::string, const Op *const>(StdOps::about.name(), &StdOps::about));
  m_ops_map->insert(std::pair<std::string, const Op *const>(StdOps::formats.name(), &StdOps::formats));
  m_ops_map->insert(std::pair<std::string, const Op *const>(StdOps::read.name(), &StdOps::read));
  m_ops_map->insert(std::pair<std::string, const Op *const>(StdOps::nav.name(), &StdOps::nav));
  m_ops_map->insert(std::pair<std::string, const Op *const>(StdOps::watch_sub.name(), &StdOps::watch_sub));
  m_ops_map->insert(std::pair<std::string, const Op *const>(StdOps::watch_unsub.name(), &StdOps::watch_unsub));
  m_ops_map->insert(std::pair<std::string, const Op *const>(StdOps::watch_poll.name(), &StdOps::watch_poll));
  m_ops_map->insert(std::pair<std::string, const Op *const>(StdOps::watch_list.name(), &StdOps::watch_list));
  m_ops_map->insert(std::pair<std::string, const Op *const>(StdOps::point_write.name(), &StdOps::point_write));
  m_ops_map->insert(std::pair<std::string, const Op *const>(StdOps::his_read.name(), &StdOps::his_read));
  m_ops_map->insert(std::pair<std::string, const Op *const>(StdOps::his_write.name(), &StdOps::his_write));
  m_ops_map->insert(std::pair<std::string, const Op *const>(StdOps::invoke_action.name(), &StdOps::invoke_action));
  m_ops_map->insert(std::pair<std::string, const Op *const>(StdOps::commit.name(), &StdOps::commit));
  m_ops_map->insert(std::pair<std::string, const Op *const>(StdOps::restart.name(), &StdOps::restart));
  m_ops_map->insert(std::pair<std::string, const Op *const>(StdOps::ops.name(), &StdOps::ops));
  m_ops_map->insert(std::pair<std::string, const Op *const>(StdOps::root.name(), &StdOps::root));

  return *m_ops_map;
}

StdOps::ops_map_t *StdOps::m_ops_map = NULL;
