#include <stdio.h>
#include <ctype.h>
#include <sstream>
#include <stdexcept>

//#include "headers.hpp"

//#include <boost/scoped_ptr.hpp>
//#include <boost/ptr_container/ptr_map.hpp>
#include <boost/ptr_container/ptr_vector.hpp>
//#include <boost/iterator/iterator_facade.hpp>

#include "dict_type.hpp"

// Log
// #include <plog/Log.h>

/***************************** NOTE *******************************************
 * Now Dict Type supports only Marker Type tags.                              *
 * Originally Dict Type is Haystack 3.0 Data Model standard and it should     *
 * support tag:value pair and markers.                                        *
 *                                                                            *
 * ToDo: Support for tag:value pair.                                          *
 *****************************************************************************/

////////////////////////////////////////////////
// DictType
////////////////////////////////////////////////
using namespace haystack;

DictType::DictType(item_t item)
{
	items = item;
}

const std::string DictType::to_string() const
{
	return to_zinc();
}

////////////////////////////////////////////////
// to zinc
////////////////////////////////////////////////

const std::string DictType::to_zinc() const
{
	std::stringstream os;
	bool first = true;

	//LOG_DEBUG << "DictType size:" << items.size();
	
	os << "{";

	for (item_t::const_iterator it = items.begin(), e = items.end(); it != e; ++it)
	{
		//Val* val = { new_clone(*it) };

		if (first)
		{
			first = false;
		}
		else
		{
			os << ' ';
		}			

		//os << val->to_zinc();
		os << *it;
		//LOG_DEBUG << "DictType loop:" << val->to_string();
	}

	os << '}';

	//LOG_DEBUG << "DictType:" << os.str();
	return os.str();
}

////////////////////////////////////////////////
// Equal DENKO
////////////////////////////////////////////////
bool DictType::operator ==(const DictType &other) const
{

	for (item_t::const_iterator t_it = items.begin(), t_e = items.end(), o_it = other.items.begin(), o_e = other.items.end(); t_it != t_e && o_it != o_e; ++t_it, o_it++)
	{
		//Val* t_val = { new_clone(*t_it) };			
		//Val* o_val = { new_clone(*o_it) };			

		//if(!(t_val == o_val))
		if(!(*t_it == *o_it))
		{
			return false;
		}

	}

    return true;
}

bool DictType::operator==(const Val &other) const
{
    // TODO: Not yet implemented
    return false;
}

bool DictType::operator ==(const std::string &other) const
{
    // TODO: Not yet implemented
    return false;
}


bool DictType::operator < (const Val &other) const
{
    // TODO: Not yet implemented
    return false;
}

bool DictType::operator >(const Val &other) const
{
    // TODO: Not yet implemented
    return false;
}


DictType::auto_ptr_t DictType::clone() const
{
    return auto_ptr_t(new DictType(*this));
}
