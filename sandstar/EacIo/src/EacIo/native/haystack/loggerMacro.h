// file : loggerMacro.h
// author : Manthan R. Tilva
// date : 11-06-2018
// History : First implimentation
//          : Adding support for filters 14-06-2018

#include "logger.h"

#ifndef haystack_LoggerMacro_h_
#define haystack_LoggerMacro_h_

#define HAYSTACK_TRACE                                                         \
  haystack::Logger::getInstance().debug("%s:%d", __FILE__, __LINE__);
#define HAYSTACK_TRACE_WITH_MSG(fomrat, ...)                                   \
  haystack::Logger::getInstance().debug(fomrat " [%s:%d]", __VA_ARGS__,        \
                                        __FILE__, __LINE__);
#define HAYSTACK_TRACE_WITH_STR(str)                                           \
  haystack::Logger::getInstance().debug("%s [%s:%d]", str, __FILE__, __LINE__);

// #define HAYSTACK_LOG_DEBUG_STR(str)                                            \
//   haystack::Logger::getInstance().debug("%s", str);
// #define HAYSTACK_LOG_DEBUG_MSG(fomrat, ...)                                    \
//   haystack::Logger::getInstance().debug(fomrat, __VA_ARGS__);

// #define HAYSTACK_LOG_INFO_STR(str)                                             \
//   haystack::Logger::getInstance().info("%s", str);
// #define HAYSTACK_LOG_INFO_MSG(fomrat, ...)                                     \
//   haystack::Logger::getInstance().info(fomrat, ##__VA_ARGS__);

// #define HAYSTACK_LOG_NOTICE_STR(str)                                           \
//   haystack::Logger::getInstance().notice("%s", str);
// #define HAYSTACK_LOG_NOTICE_MSG(fomrat, ...)                                   \
//   haystack::Logger::getInstance().notice(fomrat, __VA_ARGS__);

// #define HAYSTACK_LOG_WARNING_STR(str)                                          \
//   haystack::Logger::getInstance().warning("%s", str);
// #define HAYSTACK_LOG_WARNING_MSG(fomrat, ...)                                  \
//   haystack::Logger::getInstance().warning(fomrat, __VA_ARGS__);

// #define HAYSTACK_LOG_ERR_STR(str)                                              \
//   haystack::Logger::getInstance().err("%s", str);
// #define HAYSTACK_LOG_ERR_MSG(fomrat, ...)                                      \
//   haystack::Logger::getInstance().err(fomrat, __VA_ARGS__);

// #define HAYSTACK_LOG_CRIT_STR(str)                                             \
//   haystack::Logger::getInstance().crit("%s", str);
// #define HAYSTACK_LOG_CRIT_MSG(fomrat, ...)                                     \
//   haystack::Logger::getInstance().crit(fomrat, __VA_ARGS__);

// #define HAYSTACK_LOG_ALERT_STR(str)                                            \
//   haystack::Logger::getInstance().alert("%s", str);
// #define HAYSTACK_LOG_ALERT_MSG(fomrat, ...)                                    \
//   haystack::Logger::getInstance().alert(fomrat, __VA_ARGS__);

// #define HAYSTACK_LOG_EMERG_STR(str)                                            \
//   haystack::Logger::getInstance().emerg("%s", str);
// #define HAYSTACK_LOG_EMERG_MSG(fomrat, ...)                                    \
//   haystack::Logger::getInstance().emerg(fomrat, __VA_ARGS__);

/****************************ENGINE********************************************/
#define LOG_PRIORITY_FILTER_MSG(filter, priority, fomrat, ...)                 \
  haystack::Logger::getInstance(filter)->priority(fomrat, ##__VA_ARGS__);

#define LOG_DEBUG_ENGINE_MSG(fomrat, ...)                                      \
  LOG_PRIORITY_FILTER_MSG(haystack::Logger::LoggerFilter::ENGINE, debug,       \
                          fomrat, ##__VA_ARGS__);
#define LOG_INFO_ENGINE_MSG(fomrat, ...)                                       \
  LOG_PRIORITY_FILTER_MSG(haystack::Logger::LoggerFilter::ENGINE, info,        \
                          fomrat, ##__VA_ARGS__);
#define LOG_NOTICE_ENGINE_MSG(fomrat, ...)                                     \
  LOG_PRIORITY_FILTER_MSG(haystack::Logger::LoggerFilter::ENGINE, notice,      \
                          fomrat, ##__VA_ARGS__);
#define LOG_WARNING_ENGINE_MSG(fomrat, ...)                                    \
  LOG_PRIORITY_FILTER_MSG(haystack::Logger::LoggerFilter::ENGINE, warning,     \
                          fomrat, ##__VA_ARGS__);
#define LOG_ERR_ENGINE_MSG(fomrat, ...)                                        \
  LOG_PRIORITY_FILTER_MSG(haystack::Logger::LoggerFilter::ENGINE, err, fomrat, \
                          ##__VA_ARGS__);
#define LOG_CRIT_ENGINE_MSG(fomrat, ...)                                       \
  LOG_PRIORITY_FILTER_MSG(haystack::Logger::LoggerFilter::ENGINE, crit,        \
                          fomrat, ##__VA_ARGS__);
#define LOG_ALERT_ENGINE_MSG(fomrat, ...)                                      \
  LOG_PRIORITY_FILTER_MSG(haystack::Logger::LoggerFilter::ENGINE, alert,       \
                          fomrat, ##__VA_ARGS__);
#define LOG_EMERG_ENGINE_MSG(fomrat, ...)                                      \
  LOG_PRIORITY_FILTER_MSG(haystack::Logger::LoggerFilter::ENGINE, emerg,       \
                          fomrat, ##__VA_ARGS__);


/****************************LOGIN********************************************/
#define LOG_DEBUG_LOGIN_MSG(fomrat, ...)                                      \
  LOG_PRIORITY_FILTER_MSG(haystack::Logger::LoggerFilter::LOGIN, debug,       \
                          fomrat, ##__VA_ARGS__);
#define LOG_INFO_LOGIN_MSG(fomrat, ...)                                       \
  LOG_PRIORITY_FILTER_MSG(haystack::Logger::LoggerFilter::LOGIN, info,        \
                          fomrat, ##__VA_ARGS__);
#define LOG_NOTICE_LOGIN_MSG(fomrat, ...)                                     \
  LOG_PRIORITY_FILTER_MSG(haystack::Logger::LoggerFilter::LOGIN, notice,      \
                          fomrat, ##__VA_ARGS__);
#define LOG_WARNING_LOGIN_MSG(fomrat, ...)                                    \
  LOG_PRIORITY_FILTER_MSG(haystack::Logger::LoggerFilter::LOGIN, warning,     \
                          fomrat, ##__VA_ARGS__);
#define LOG_ERR_LOGIN_MSG(fomrat, ...)                                        \
  LOG_PRIORITY_FILTER_MSG(haystack::Logger::LoggerFilter::LOGIN, err, fomrat, \
                          ##__VA_ARGS__);
#define LOG_CRIT_LOGIN_MSG(fomrat, ...)                                       \
  LOG_PRIORITY_FILTER_MSG(haystack::Logger::LoggerFilter::LOGIN, crit,        \
                          fomrat, ##__VA_ARGS__);
#define LOG_ALERT_LOGIN_MSG(fomrat, ...)                                      \
  LOG_PRIORITY_FILTER_MSG(haystack::Logger::LoggerFilter::LOGIN, alert,       \
                          fomrat, ##__VA_ARGS__);
#define LOG_EMERG_LOGIN_MSG(fomrat, ...)                                      \
  LOG_PRIORITY_FILTER_MSG(haystack::Logger::LoggerFilter::LOGIN, emerg,       \
                          fomrat, ##__VA_ARGS__);

/****************************USERS********************************************/
#define LOG_DEBUG_USERS_MSG(fomrat, ...)                                      \
  LOG_PRIORITY_FILTER_MSG(haystack::Logger::LoggerFilter::USERS, debug,       \
                          fomrat, ##__VA_ARGS__);
#define LOG_INFO_USERS_MSG(fomrat, ...)                                       \
  LOG_PRIORITY_FILTER_MSG(haystack::Logger::LoggerFilter::USERS, info,        \
                          fomrat, ##__VA_ARGS__);
#define LOG_NOTICE_USERS_MSG(fomrat, ...)                                     \
  LOG_PRIORITY_FILTER_MSG(haystack::Logger::LoggerFilter::USERS, notice,      \
                          fomrat, ##__VA_ARGS__);
#define LOG_WARNING_USERS_MSG(fomrat, ...)                                    \
  LOG_PRIORITY_FILTER_MSG(haystack::Logger::LoggerFilter::USERS, warning,     \
                          fomrat, ##__VA_ARGS__);
#define LOG_ERR_USERS_MSG(fomrat, ...)                                        \
  LOG_PRIORITY_FILTER_MSG(haystack::Logger::LoggerFilter::USERS, err, fomrat, \
                          ##__VA_ARGS__);
#define LOG_CRIT_USERS_MSG(fomrat, ...)                                       \
  LOG_PRIORITY_FILTER_MSG(haystack::Logger::LoggerFilter::USERS, crit,        \
                          fomrat, ##__VA_ARGS__);
#define LOG_ALERT_USERS_MSG(fomrat, ...)                                      \
  LOG_PRIORITY_FILTER_MSG(haystack::Logger::LoggerFilter::USERS, alert,       \
                          fomrat, ##__VA_ARGS__);
#define LOG_EMERG_USERS_MSG(fomrat, ...)                                      \
  LOG_PRIORITY_FILTER_MSG(haystack::Logger::LoggerFilter::USERS, emerg,       \
                          fomrat, ##__VA_ARGS__);

/****************************HAYSTACKOPS********************************************/
#define LOG_DEBUG_HAYSTACKOPS_MSG(fomrat, ...)                                      \
  LOG_PRIORITY_FILTER_MSG(haystack::Logger::LoggerFilter::HAYSTACKOPS, debug,       \
                          fomrat, ##__VA_ARGS__);
#define LOG_INFO_HAYSTACKOPS_MSG(fomrat, ...)                                       \
  LOG_PRIORITY_FILTER_MSG(haystack::Logger::LoggerFilter::HAYSTACKOPS, info,        \
                          fomrat, ##__VA_ARGS__);
#define LOG_NOTICE_HAYSTACKOPS_MSG(fomrat, ...)                                     \
  LOG_PRIORITY_FILTER_MSG(haystack::Logger::LoggerFilter::HAYSTACKOPS, notice,      \
                          fomrat, ##__VA_ARGS__);
#define LOG_WARNING_HAYSTACKOPS_MSG(fomrat, ...)                                    \
  LOG_PRIORITY_FILTER_MSG(haystack::Logger::LoggerFilter::HAYSTACKOPS, warning,     \
                          fomrat, ##__VA_ARGS__);
#define LOG_ERR_HAYSTACKOPS_MSG(fomrat, ...)                                        \
  LOG_PRIORITY_FILTER_MSG(haystack::Logger::LoggerFilter::HAYSTACKOPS, err, fomrat, \
                          ##__VA_ARGS__);
#define LOG_CRIT_HAYSTACKOPS_MSG(fomrat, ...)                                       \
  LOG_PRIORITY_FILTER_MSG(haystack::Logger::LoggerFilter::HAYSTACKOPS, crit,        \
                          fomrat, ##__VA_ARGS__);
#define LOG_ALERT_HAYSTACKOPS_MSG(fomrat, ...)                                      \
  LOG_PRIORITY_FILTER_MSG(haystack::Logger::LoggerFilter::HAYSTACKOPS, alert,       \
                          fomrat, ##__VA_ARGS__);
#define LOG_EMERG_HAYSTACKOPS_MSG(fomrat, ...)                                      \
  LOG_PRIORITY_FILTER_MSG(haystack::Logger::LoggerFilter::HAYSTACKOPS, emerg,       \
                          fomrat, ##__VA_ARGS__);

/****************************POINTWRITE********************************************/
#define LOG_DEBUG_POINTWRITE_MSG(fomrat, ...)                                      \
  LOG_PRIORITY_FILTER_MSG(haystack::Logger::LoggerFilter::POINTWRITE, debug,       \
                          fomrat, ##__VA_ARGS__);
#define LOG_INFO_POINTWRITE_MSG(fomrat, ...)                                       \
  LOG_PRIORITY_FILTER_MSG(haystack::Logger::LoggerFilter::POINTWRITE, info,        \
                          fomrat, ##__VA_ARGS__);
#define LOG_NOTICE_POINTWRITE_MSG(fomrat, ...)                                     \
  LOG_PRIORITY_FILTER_MSG(haystack::Logger::LoggerFilter::POINTWRITE, notice,      \
                          fomrat, ##__VA_ARGS__);
#define LOG_WARNING_POINTWRITE_MSG(fomrat, ...)                                    \
  LOG_PRIORITY_FILTER_MSG(haystack::Logger::LoggerFilter::POINTWRITE, warning,     \
                          fomrat, ##__VA_ARGS__);
#define LOG_ERR_POINTWRITE_MSG(fomrat, ...)                                        \
  LOG_PRIORITY_FILTER_MSG(haystack::Logger::LoggerFilter::POINTWRITE, err, fomrat, \
                          ##__VA_ARGS__);
#define LOG_CRIT_POINTWRITE_MSG(fomrat, ...)                                       \
  LOG_PRIORITY_FILTER_MSG(haystack::Logger::LoggerFilter::POINTWRITE, crit,        \
                          fomrat, ##__VA_ARGS__);
#define LOG_ALERT_POINTWRITE_MSG(fomrat, ...)                                      \
  LOG_PRIORITY_FILTER_MSG(haystack::Logger::LoggerFilter::POINTWRITE, alert,       \
                          fomrat, ##__VA_ARGS__);
#define LOG_EMERG_POINTWRITE_MSG(fomrat, ...)                                      \
  LOG_PRIORITY_FILTER_MSG(haystack::Logger::LoggerFilter::POINTWRITE, emerg,       \
                          fomrat, ##__VA_ARGS__);

/****************************IPCS********************************************/
#define LOG_DEBUG_IPCS_MSG(fomrat, ...)                                      \
  LOG_PRIORITY_FILTER_MSG(haystack::Logger::LoggerFilter::IPCS, debug,       \
                          fomrat, ##__VA_ARGS__);
#define LOG_INFO_IPCS_MSG(fomrat, ...)                                       \
  LOG_PRIORITY_FILTER_MSG(haystack::Logger::LoggerFilter::IPCS, info,        \
                          fomrat, ##__VA_ARGS__);
#define LOG_NOTICE_IPCS_MSG(fomrat, ...)                                     \
  LOG_PRIORITY_FILTER_MSG(haystack::Logger::LoggerFilter::IPCS, notice,      \
                          fomrat, ##__VA_ARGS__);
#define LOG_WARNING_IPCS_MSG(fomrat, ...)                                    \
  LOG_PRIORITY_FILTER_MSG(haystack::Logger::LoggerFilter::IPCS, warning,     \
                          fomrat, ##__VA_ARGS__);
#define LOG_ERR_IPCS_MSG(fomrat, ...)                                        \
  LOG_PRIORITY_FILTER_MSG(haystack::Logger::LoggerFilter::IPCS, err, fomrat, \
                          ##__VA_ARGS__);
#define LOG_CRIT_IPCS_MSG(fomrat, ...)                                       \
  LOG_PRIORITY_FILTER_MSG(haystack::Logger::LoggerFilter::IPCS, crit,        \
                          fomrat, ##__VA_ARGS__);
#define LOG_ALERT_IPCS_MSG(fomrat, ...)                                      \
  LOG_PRIORITY_FILTER_MSG(haystack::Logger::LoggerFilter::IPCS, alert,       \
                          fomrat, ##__VA_ARGS__);
#define LOG_EMERG_IPCS_MSG(fomrat, ...)                                      \
  LOG_PRIORITY_FILTER_MSG(haystack::Logger::LoggerFilter::IPCS, emerg,       \
                          fomrat, ##__VA_ARGS__);

/****************************UNNAMED********************************************/
#define LOG_DEBUG_UNNAMED_MSG(fomrat, ...)                                      \
  LOG_PRIORITY_FILTER_MSG(haystack::Logger::LoggerFilter::UNNAMED, debug,       \
                          fomrat, ##__VA_ARGS__);
#define LOG_INFO_UNNAMED_MSG(fomrat, ...)                                       \
  LOG_PRIORITY_FILTER_MSG(haystack::Logger::LoggerFilter::UNNAMED, info,        \
                          fomrat, ##__VA_ARGS__);
#define LOG_NOTICE_UNNAMED_MSG(fomrat, ...)                                     \
  LOG_PRIORITY_FILTER_MSG(haystack::Logger::LoggerFilter::UNNAMED, notice,      \
                          fomrat, ##__VA_ARGS__);
#define LOG_WARNING_UNNAMED_MSG(fomrat, ...)                                    \
  LOG_PRIORITY_FILTER_MSG(haystack::Logger::LoggerFilter::UNNAMED, warning,     \
                          fomrat, ##__VA_ARGS__);
#define LOG_ERR_UNNAMED_MSG(fomrat, ...)                                        \
  LOG_PRIORITY_FILTER_MSG(haystack::Logger::LoggerFilter::UNNAMED, err, fomrat, \
                          ##__VA_ARGS__);
#define LOG_CRIT_UNNAMED_MSG(fomrat, ...)                                       \
  LOG_PRIORITY_FILTER_MSG(haystack::Logger::LoggerFilter::UNNAMED, crit,        \
                          fomrat, ##__VA_ARGS__);
#define LOG_ALERT_UNNAMED_MSG(fomrat, ...)                                      \
  LOG_PRIORITY_FILTER_MSG(haystack::Logger::LoggerFilter::UNNAMED, alert,       \
                          fomrat, ##__VA_ARGS__);
#define LOG_EMERG_UNNAMED_MSG(fomrat, ...)                                      \
  LOG_PRIORITY_FILTER_MSG(haystack::Logger::LoggerFilter::UNNAMED, emerg,       \
                          fomrat, ##__VA_ARGS__);

/****************************STARTUP********************************************/
#define LOG_DEBUG_STARTUP_MSG(fomrat, ...)                                      \
  LOG_PRIORITY_FILTER_MSG(haystack::Logger::LoggerFilter::STARTUP, debug,       \
                          fomrat, ##__VA_ARGS__);
#define LOG_INFO_STARTUP_MSG(fomrat, ...)                                       \
  LOG_PRIORITY_FILTER_MSG(haystack::Logger::LoggerFilter::STARTUP, info,        \
                          fomrat, ##__VA_ARGS__);
#define LOG_NOTICE_STARTUP_MSG(fomrat, ...)                                     \
  LOG_PRIORITY_FILTER_MSG(haystack::Logger::LoggerFilter::STARTUP, notice,      \
                          fomrat, ##__VA_ARGS__);
#define LOG_WARNING_STARTUP_MSG(fomrat, ...)                                    \
  LOG_PRIORITY_FILTER_MSG(haystack::Logger::LoggerFilter::STARTUP, warning,     \
                          fomrat, ##__VA_ARGS__);
#define LOG_ERR_STARTUP_MSG(fomrat, ...)                                        \
  LOG_PRIORITY_FILTER_MSG(haystack::Logger::LoggerFilter::STARTUP, err, fomrat, \
                          ##__VA_ARGS__);
#define LOG_CRIT_STARTUP_MSG(fomrat, ...)                                       \
  LOG_PRIORITY_FILTER_MSG(haystack::Logger::LoggerFilter::STARTUP, crit,        \
                          fomrat, ##__VA_ARGS__);
#define LOG_ALERT_STARTUP_MSG(fomrat, ...)                                      \
  LOG_PRIORITY_FILTER_MSG(haystack::Logger::LoggerFilter::STARTUP, alert,       \
                          fomrat, ##__VA_ARGS__);
#define LOG_EMERG_STARTUP_MSG(fomrat, ...)                                      \
  LOG_PRIORITY_FILTER_MSG(haystack::Logger::LoggerFilter::STARTUP, emerg,       \
                          fomrat, ##__VA_ARGS__);
#endif // haystack_LoggerMacro_h_
