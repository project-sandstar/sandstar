#pragma once

#include "val.hpp"


/***************************** NOTE *******************************************
 * Now Dict Type supports only Marker Type tags.                              *
 * Originally Dict Type is Haystack 3.0 Data Model standard and it should     *
 * support tag:value pair and markers.                                        *
 *                                                                            *
 * ToDo: Support for tag:value pair.                                          *
 *****************************************************************************/
 

namespace haystack {
	/**
	 * DictType is an immutable list of Marker type items.
	 */
    class DictType : public Val
    {
        DictType();

        // disable assignment
        DictType& operator = (const DictType &other);
        DictType(const DictType& other) : items(other.items) {};
		
    public:
	
		typedef std::vector<std::string> item_t;
		typedef item_t::const_iterator const_iterator;
        
		const Type type() const { return DICT_TYPE; }
		
		item_t items;
		
        /**
        Construct from std::string
        */
		DictType(item_t);

        /**
        MIME type
        */
        const std::string to_string() const;

        /**
        Encode as Type("Value")
        */
        const std::string to_zinc() const;

        /**
        Equality is value based
        */
        bool operator == (const DictType &b) const;
	    bool operator == (const Val &other) const;
	    bool operator == (const std::string &other) const;

		/*DENKO*/
	    bool operator > (const Val &other) const;
        bool operator < (const Val &other) const;
        

        auto_ptr_t clone() const;
    };
};
