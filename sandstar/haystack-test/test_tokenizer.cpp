#include <string>
#include <cstring>
#include <vector>
#include "io/token.hpp"
#include "io/tokenizer.hpp"
#include "ref.hpp"
#include "num.hpp"
#include "date.hpp"
#include "time.hpp"
#include "timezone.hpp"
#include "datetime.hpp"
#include "uri.hpp"
#include "ext/catch/catch.hpp"


using namespace haystack;

typedef std::vector<std::pair<Token::ptr_t, const Val*> > Toks;
//typedef std::vector<Token::ptr_t> Toks;

void verifyToks(const std::string& zinc, const Toks& toks) {
	//  std::vector<Token> acc;

	std::istringstream ss( zinc );

	Tokenizer actual( ss );
	//  int i = 0;
	Toks::const_iterator expected = toks.begin( );

	while ( true ) {
		Token::ptr_t x = actual.next( );
		CHECK( *x == *actual.tok );
		if ( *x == *Token::eof ) break;
		CHECK( *expected->first == *actual.tok );
		CHECK( *expected->second == *actual.val );
		expected++;
	}
}

inline int tzOffset(int hours, int mins) { return (hours * 3600) + (mins * 60); }

TEST_CASE("Tokenizer", "[Tokenizer]") {

    SECTION("testEmpty") {
        verifyToks("", {} );
    }

	SECTION("testId") {
	  verifyToks("x", {{Token::id, new Ref("x")}});
	  verifyToks("fooBar", {{Token::id, new Ref("fooBar")}});
	  verifyToks("fooBar1999x", {{Token::id, new Ref("fooBar1999x")}});
	  verifyToks("foo_23", {{Token::id, new Ref("foo_23")}});
	  verifyToks("Foo", {{Token::id, new Ref("Foo")}});
	}

	SECTION("testInts") {
		verifyToks("5", {{Token::num, new Num(5) }});
		verifyToks("0x1234_abcd", {{Token::num, new Num(0x1234abcd) }});
	}

	SECTION( "testFloats" ) {
		verifyToks( "5.0", {{ Token::num, new Num( 5. ) }} );
		verifyToks( "5.42", {{ Token::num, new Num( 5.42 ) }} );
		verifyToks( "123.2e32", {{ Token::num, new Num( 123.2e32 ) }} );
		verifyToks( "123.2e+32", {{ Token::num, new Num( 123.2e32 ) }} );
		verifyToks( "2_123.2e+32", {{ Token::num, new Num( 2123.2e32 ) }} );
		verifyToks( "4.2e-7", {{ Token::num, new Num( 4.2e-7 ) }} );
	}

	SECTION("testNumberWithUnits") {
		verifyToks("-40ms", { {Token::num, new Num(-40, "ms")}});
		verifyToks("1sec", { {Token::num, new Num(1, "sec")}});
		verifyToks("5hr", { {Token::num, new Num(5, "hr")}});
		verifyToks("2.5day", { {Token::num, new Num(2.5, "day")}});
		verifyToks("12%", { {Token::num, new Num(12, "%")}});
		verifyToks("987_foo", { {Token::num, new Num(987, "_foo")}});
		verifyToks("-1.2m/s", { {Token::num, new Num(-1.2, "m/s")}});
		verifyToks("12kWh/ft\u00B2", { {Token::num, new Num(12, "kWh/ft\u00B2")}});
		verifyToks("3_000.5J/kg_dry", { {Token::num, new Num(3000.5, "J/kg_dry")}});
	}

	SECTION("testStrings") {
		verifyToks("\"\"", { {Token::str, new Str("")}});
		verifyToks("\"x y\"", { {Token::str, new Str("x y")}});
		verifyToks("\"x\\\"y\"", { {Token::str, new Str("x\"y")}});
		verifyToks("\"_\\u012f \\n \\t \\\\_\"", { {Token::str, new Str("_\u012f \n \t \\_")}});
	}

	SECTION("testDate") {
		verifyToks("2016-06-06", {{Token::date, new Date(2016, 6, 6)}});
	}

	SECTION("testTime") {
		verifyToks("8:30", { {Token::time, new Time(8,30)}});
		verifyToks("20:15", { {Token::time, new Time(20,15)}});
		verifyToks("00:00", { {Token::time, new Time(0,0)}});
		verifyToks("00:00:00", { {Token::time, new Time(0,0,0)}});
		verifyToks("01:02:03", { {Token::time, new Time(1,2,3)}});
		verifyToks("23:59:59", { {Token::time, new Time(23,59,59)}});
		verifyToks("12:00:12.9", { {Token::time, new Time(12,00,12,900)}});
		verifyToks("12:00:12.99", { {Token::time, new Time(12,00,12,990)}});
		verifyToks("12:00:12.999", { {Token::time, new Time(12,00,12,999)}});
		verifyToks("12:00:12.000", { {Token::time, new Time(12,00,12,0)}});
		verifyToks("12:00:12.001", { {Token::time, new Time(12,00,12,1)}});
	}

	SECTION("testDateTime") {
		TimeZone ny("New_York");
		TimeZone utc = TimeZone::UTC;
		TimeZone london("London");
		verifyToks("2016-01-13T09:51:33-05:00 New_York", { {Token::dateTime, new DateTime(2016,1,13,9,51,33, ny, tzOffset(-5,0))}});
		verifyToks("2016-01-13T09:51:33.353-05:00 New_York", { {Token::dateTime, new DateTime(Date(2016,1,13), Time(9,51,33,353), ny, tzOffset(-5,0))}});
		verifyToks("2010-12-18T14:11:30.924Z", { {Token::dateTime, new DateTime(Date(2010,12,18), Time(14,11,30,924), utc)}});
		verifyToks("2010-12-18T14:11:30.924Z UTC", { {Token::dateTime, new DateTime(Date(2010,12,18), Time(14,11,30,924), utc)}});
		verifyToks("2010-12-18T14:11:30.924Z London", { {Token::dateTime, new DateTime(Date(2010,12,18), Time(14,11,30,924), london)}});
		// Apparently PST8PDT is not valid in java?
	//    verifyToks("2015-01-02T06:13:38.701-08:00 PST8PDT", { {Token::dateTime, new DateTime(new Date(2015,1,2), new Time(6,13,38,701), HTimeZone.make("PST8PDT"), tzOffset(-8,0))}});
		verifyToks("2010-03-01T23:55:00.013-05:00 GMT+5", { {Token::dateTime, new DateTime(Date(2010,3,1), Time(23,55,0,13), TimeZone("GMT+5"), tzOffset(-5,0))}});
		verifyToks("2010-03-01T23:55:00.013+10:00 GMT-10 ", { {Token::dateTime, new DateTime(Date(2010,3,1), Time(23,55,0,13), TimeZone("GMT-10"), tzOffset(10,0))}});
	}

	SECTION("testRef") {
		verifyToks("@125b780e-0684e169", { {Token::ref, new Ref("125b780e-0684e169")}});
		verifyToks("@demo:_:-.~", { {Token::ref, new Ref("demo:_:-.~")}});
	}

	SECTION("testUri") {
	  verifyToks("`http://foo/`", { {Token::uri, new Uri("http://foo/")}});
	  verifyToks("`_ \\n \\\\ \\`_`", { {Token::uri, new Uri("_ \n \\\\ `_")}});
	}

	SECTION("testWhitespace") {

	  verifyToks("a\n  b   \rc \r\nd\n\ne",
	  {
		
		{ Token::id, new Ref("a") },
		{ Token::nl, &EmptyVal::DEF },
		{ Token::id, new Ref("b") },
		{ Token::nl, &EmptyVal::DEF },
		{ Token::id, new Ref("c") },
		{ Token::nl, &EmptyVal::DEF },
		{ Token::id, new Ref("d") },
		{ Token::nl, &EmptyVal::DEF },
		{ Token::nl, &EmptyVal::DEF },
		{ Token::id, new Ref("e") }
	  } );
	}
}