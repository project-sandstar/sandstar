# Preparing dev box
## Packages
    aptitude -y install mercurial libboost-dev libpoco-dev
    pip install env

## Source code
    WS=~/src #or urs
    mkdir -p $WS
    cd $WS
    git clone https://github.com/SergiusTheBest/plog.git
    ln -sT $WS/plog/include/plog/ /usr/include/plog

    hg clone https://bitbucket.org/bassg/eac-tools
    ln -s $WS/eac-tools/SedonaHaystackBuilds/EacIo ~/sedonadev

## bash profile
Put in your ~/.bashrc:

	export JAVA_HOME=$HOME/opt/jdk1.8.0_60/
	. $HOME/sedonadev/adm/unix/init.sh
	export sedona_home=$HOME/sedonadev

## Building project sources
Every time you rebuild with:

    makeunixvm -p platforms/src/generic/unix/eac.xml
