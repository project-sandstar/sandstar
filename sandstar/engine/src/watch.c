// watch.c

// Include files

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "global.h"
#include "engine.h"
#include "table.h"
#include "value.h"
#include "channel.h"
#include "watch.h"
#include "notify.h"
#include "poll.h"

// Local functions

static int watch_find(WATCH *watch,ENGINE_CHANNEL channel,key_t sender);
static int watch_next(WATCH *watch);

// Public functions

// watch_init
// Call to initialize watch controller

int watch_init(WATCH *watch,int nItems)
{
	int nSize=nItems*sizeof(WATCH_ITEM);

	watch->nItems=nItems;
	watch->nCount=0;

	watch->items=(WATCH_ITEM *) malloc(nSize);
	memset(watch->items,0,nSize);

	return 0;
}

// watch_exit
// Call to release watch controller

int watch_exit(WATCH *watch)
{
	// free items?
	if(watch->items!=NULL)
	{
		free(watch->items);
		watch->items=NULL;
	}

	watch->nItems=0;
	watch->nCount=0;

	return 0;
}

// watch_add
// Call to add item to watch list

int watch_add(WATCH *watch,ENGINE_CHANNEL channel,key_t sender)
{
	// already watching?
	int n=watch_find(watch,channel,sender);
	if(n>=0) return 0;

	// get next free
	n=watch_next(watch);

	if(n<0)
	{
		engine_error("out of watch space");
		return -1;
	}

	// enable watch item
	WATCH_ITEM *item=&watch->items[n];

	item->nUsed=1;

	item->channel=channel;
	item->sender=sender;

	// add to count
	watch->nCount++;

	return 0;
}

// watch_remove
// Call to remove watch item

int watch_remove(WATCH *watch,ENGINE_CHANNEL channel,key_t sender)
{
	// already watching?
	int n=watch_find(watch,channel,sender);

	if(n>=0)
	{
		// mark as unused
		watch->items[n].nUsed=0;
		watch->nCount--;

		return 0;
	}
	return -1;
}

// watch_send
// Call to send watch message

int watch_send(key_t sender,ENGINE_CHANNEL channel,ENGINE_VALUE *value)
{
	// queue found?
	int hQueue=msgget(sender,0666);

	if(hQueue>=0)
	{
		// prep message
		ENGINE_MESSAGE msg;
		memset(&msg,0,sizeof(ENGINE_MESSAGE));

		msg.nMessage=ENGINE_MESSAGE_UPDATE;
		msg.channel=channel;
		msg.value=*value;

		// send message?
		if(msgsnd(hQueue,&msg,ENGINE_MESSAGE_SIZE,0)<0)
		{
//			engine_error("failed to queue message");
//			return -1;
		}
		return 0;
	}
	// else error

	return -1;
}

// watch_update
// Call to update current watches

int watch_update(WATCH *watch,ENGINE_CHANNEL channel,ENGINE_VALUE *value)
{
	// find watchers
	int n;

	for(n=0;n<watch->nItems;n++)
	{
		WATCH_ITEM *item=&watch->items[n];

		// watch found?
		if(item->nUsed!=0 && item->channel==channel)
			watch_send(item->sender,channel,value);
	}
	return 0;
}

// watch_report
// Call to display list of active watches

int watch_report(WATCH *watch)
{
	// output header
	printf("\nchannel sender\n");
	printf("------- ----------\n");

	// output watch items
	int n;

	for(n=0;n<watch->nItems;n++)
	{
		WATCH_ITEM *item=&watch->items[n];

		// item in use?
		if(item->nUsed!=0)
		{
			printf("%-7d 0x%-8.8X\n",
				item->channel,
				item->sender);
		}
	}
	printf("\n");

	return 0;
}

// Local functions

// watch_find
// Call to find existing watch item

static int watch_find(WATCH *watch,ENGINE_CHANNEL channel,key_t sender)
{
	int n;

	for(n=0;n<watch->nItems;n++)
	{
		// item found?
		WATCH_ITEM *item=&watch->items[n];

		if(item->nUsed!=0 && item->sender==sender &&
			item->channel==channel)
			return n;
	}
	return -1;
}

// watch_next
// Call to find first free watch item

static int watch_next(WATCH *watch)
{
	int n;

	for(n=0;n<watch->nItems;n++)
	{
		// item not in use?
		if(watch->items[n].nUsed==0)
			return n;
	}
	return -1;
}
