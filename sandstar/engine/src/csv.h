// csv.h

#ifndef __CSV_H__
#define __CSV_H__

// CSV grid

struct _CSV
{
	char *pData;
	char *pParse;

	int nRows;
	int nColumns;

	char **sColumns;
	char **sValues;
};

typedef struct _CSV CSV;

// Public functions

int csv_init(CSV *csv);
int csv_exit(CSV *csv);

int csv_load(CSV *csv,char *sFile);

int csv_column(CSV *csv,char *sColumn);
int csv_value(CSV *csv,int nRow,int nColumn,char **sData);
int csv_list(CSV *csv,int nRow,char *sColumn,char *sList[]);

int csv_integer(CSV *csv,int nRow,char *sColumn);
char *csv_string(CSV *csv,int nRow,char *sColumn);

int csv_report(CSV *csv,char *sFile);
int csv_report_columns(CSV *csv);
int csv_report_values(CSV *csv,char *sColumns);

#endif // _CSV_H__
