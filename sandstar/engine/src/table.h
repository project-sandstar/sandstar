// tables.h

#ifndef __TABLE_H__
#define __TABLE_H__

// Definitions

#define MAX_TABLES		16

#define MAX_TABLETAG	16
#define MAX_TABLEUNIT	16
#define MAX_TABLEPATH	128

// Typedefs

typedef double TABLE_VALUE;

// Table item

struct _TABLE_ITEM
{
	int nUsed;

	char sTag[MAX_TABLETAG+1];
	char sUnit[MAX_TABLEUNIT+1];
	char sPath[MAX_TABLEPATH+1];

	TABLE_VALUE *values;
	int nValues;

	int nDirection;
};

typedef struct _TABLE_ITEM TABLE_ITEM;

// Tables struct

struct _TABLE
{
	int nItems;
	int nCount;

	TABLE_ITEM *items;
};

typedef struct _TABLE TABLE;

// Public functions

int table_init(TABLE *tables,int nItems);
int table_exit(TABLE *tables);

int table_load(TABLE *tables,char *sFile);
int table_add(TABLE *tables,char *sTag,char *sUnit,char *sPath);

int table_report(TABLE *tables);

#endif // __TABLE_H__
