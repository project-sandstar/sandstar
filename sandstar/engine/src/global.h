// global.h

#ifndef __GLOBAL_H__
#define __GLOBAL_H__

// Includes

#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>

// Public functions

int engine_error(char *sFormat,...);

#endif // __GLOBAL_H__
