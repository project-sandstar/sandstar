// csv.c

// Include files

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>

#include "global.h"
#include "csv.h"

// Definitions

#define MAX_COLUMN		32
#define MAX_COLS		10

// Local function definitions

static int csv_decode_columns(CSV *csv);
static int csv_decode_values(CSV *csv);

static int csv_next_line(CSV *csv);
static int csv_skip_blank(CSV *csv);

// Public functions

// csv_init
// Call to initialize csv

int csv_init(CSV *csv)
{
	memset(csv,0,sizeof(CSV));
	return 0;
}

// csv_exit
// Call to release csv grid

int csv_exit(CSV *csv)
{
	// free tags
	if(csv->sColumns!=NULL)
	{
		free(csv->sColumns);
		csv->sColumns=NULL;
	}

	// free grid data
	if(csv->sValues!=NULL)
	{
		free(csv->sValues);
		csv->sValues=NULL;
	}

	// free file data
	if(csv->pData!=NULL)
	{
		free(csv->pData);
		csv->pData=NULL;
	}

	// reset members
	csv->pParse=NULL;

	csv->nRows=0;
	csv->nColumns=0;

	return 0;
}

// csv_load
// Call to load csv into memory

int csv_load(CSV *csv,char *sFile)
{
	// open the file?
	int hFile=open(sFile,O_RDONLY);

	if(hFile<0)
	{
		engine_error("csv file not found");
		return -1;
	}

	// get file size
	long nLength=lseek(hFile,0,SEEK_END);
	lseek(hFile,0,SEEK_SET);

	if(nLength<=0)
	{
		close(hFile);

		engine_error("csv file is empty");
		return -1;
	}

	// allocate file memory
	char *pData=(char *) malloc(nLength+1);

	if(pData==NULL)
	{
		close(hFile);

		engine_error("out of memory loading csv file");
		return -1;
	}

	// read file into memory
	long nSize=read(hFile,pData,nLength);
	close(hFile);

	if(nSize!=nLength)
	{
		free(pData);

		engine_error("csv file read error");
		return -1;
	}

	// ready for parsing
	pData[nLength]=0;

	csv->pData=pData;
	csv->pParse=pData;

	// parse csv data
	if(csv_decode_columns(csv)==0)
		return csv_decode_values(csv);

	return -1;
}

// csv_column
// Call to get csv column index

int csv_column(CSV *csv,char *sColumn)
{
	int n;

	for(n=0;n<csv->nColumns;n++)
	{
		// column matches?
		if(strcmp(csv->sColumns[n],sColumn)==0)
			return n;
	}
	return -1;
}

// csv_value
// Call to get csv data from coordinates

int csv_value(CSV *csv,int nRow,int nColumn,char **sData)
{
	// valid row?
	if(nRow<0 || nRow>=csv->nRows)
	{
		engine_error("csv row out of range");

		*sData="";
		return -1;
	}

	// valid column?
	if(nColumn<0 || nColumn>=csv->nColumns)
	{
//		engine_error("csv column out of range");

		*sData="";
		return -1;
	}

	// set data pointer
	*sData=csv->sValues[(nRow*csv->nColumns)+nColumn];

	return 0;
}

// csv_list
// Call to get string list index from cell

int csv_list(CSV *csv,int nRow,char *sColumn,char *sList[])
{
	// get column
	int nColumn=csv_column(csv,sColumn);

	if(nColumn>=0)
	{
		// get csv data
		char *sData;
		csv_value(csv,nRow,nColumn,&sData);

		// what list entry?
		int n;

		for(n=0;sList[n]!=NULL;n++)
		{
			if(strcmp(sData,sList[n])==0)
				return n;
		}
	}
	return 0;
}

// csv_integer
// Call to get integer from cell

int csv_integer(CSV *csv,int nRow,char *sColumn)
{
	// get column
	int nColumn=csv_column(csv,sColumn);

	if(nColumn>=0)
	{
		// get csv data
		char *sData;
		csv_value(csv,nRow,nColumn,&sData);

		// convert to int
		return strtol(sData,NULL,10);
	}
	return -1;
}

// csv_string
// Call to get string from cell

char *csv_string(CSV *csv,int nRow,char *sColumn)
{
	// get column
	int nColumn=csv_column(csv,sColumn);

	if(nColumn>=0)
	{
		// get csv data
		char *sData;
		csv_value(csv,nRow,nColumn,&sData);

		return sData;
	}
	return "";
}

// csv_report
// Call to output csv file report

int csv_report(CSV *csv,char *sFile)
{
	// output file report
	printf("\n    file: %s\n",sFile);
	printf("    cols: %d\n",csv->nColumns);
	printf("    rows: %d\n\n",csv->nRows);

	return 0;
}

// csv_report_columns
// Call to output csv column list

int csv_report_columns(CSV *csv)
{
	// display columns
	int n;

	for(n=0;n<csv->nColumns;n++)
	{
		if(n%4==0) printf("\n");
		printf("%-16.16s ",csv->sColumns[n]);
	}

	printf("\n\n");

	return 0;
}

// csv_report_values
// Call to output csv file values

int csv_report_values(CSV *csv,char *sColumns)
{
	// find column names
	char sColumn[MAX_COLUMN];

	int nCol[MAX_COLS];
	int nWidth[MAX_COLS];

	int nRows=csv->nRows;
	int nCols=csv->nColumns;

	int nColumns=0;
	int n;

	for(n=0;nColumns<MAX_COLS && n<nCols;n++)
	{
		snprintf(sColumn,MAX_COLUMN,",%s,",csv->sColumns[n]);

		if(strstr(sColumns,sColumn)!=NULL)
		{
			nCol[nColumns]=n;
			nWidth[nColumns]=strlen(sColumn)-2;

			nColumns++;
		}
	}

	// found any?
	if(nColumns==0)
	{
		engine_error("no columns found");
		return -1;
	}

	// check data widths
	char sFormat[16];
	char *sData;

	int c,r;

	for(c=0;c<nColumns;c++)
	{
		for(r=0;r<nRows;r++)
		{
			csv_value(csv,r,nCol[c],&sData);

			int w=strlen(sData);
			if(w>nWidth[c]) nWidth[c]=w;
		}
	}

	// output headings
	printf("\n");

	for(c=0;c<nColumns;c++)
	{
		sprintf(sFormat,"%%-%d.%ds ",nWidth[c],nWidth[c]);
		printf(sFormat,csv->sColumns[nCol[c]]);
	}

	printf("\n");

	for(c=0;c<nColumns;c++)
	{
		for(r=0;r<nWidth[c];r++)
			printf("-");

		printf(" ");
	}

	printf("\n");

	for(r=0;r<nRows;r++)
	{
		for(c=0;c<nColumns;c++)
		{
			csv_value(csv,r,nCol[c],&sData);

			sprintf(sFormat,"%%-%d.%ds ",nWidth[c],nWidth[c]);
			printf(sFormat,sData);
		}
		printf("\n");
	}

	printf("\n");

	return 0;
}

// Local functions

// csv_decode_columns
// Call to decode csv columns

static int csv_decode_columns(CSV *csv)
{
	// skip to data
	char *l=csv->pParse;
	char *p=l;

	csv_next_line(csv);

	// count columns
	char c;

	int nColumns=0;
	int nColumn=0;

	while((c=*p)!=0 && c!='\n' && c!='\r')
	{
		while((c=*p)==' ' || c=='\t') p++;

		if(c!=0 && c!='\n' && c!='\r')
		{
			while((c=*p)!=0 && c!=',' && c!='\n' && c!='\r') p++;
			nColumns++;

			if(c==',') p++;			
		}
	}

	// must have columns
	if(nColumns==0)
	{
		engine_error("no csv columns found");
		return -1;
	}

	// allocate tags
	char **sColumns=(char **) malloc(sizeof(char *)*nColumns);

	if(sColumns==NULL)
	{
		engine_error("out of memory allocating csv columns");
		return -1;
	}

	// read column names
	p=l;

	while((c=*p)!=0 && c!='\n' && c!='\r')
	{
		// skip whitespace
		while((c=*p)==' ' || c=='\t') p++;

		if(c!=0 && c!='\n' && c!='\r')
		{
			// set tag name
			sColumns[nColumn]=p;

			// skip to next name
			while((c=*p)!=0 && c!=',' && c!='\n' && c!='\r') p++;
			nColumn++;

			*p=0;

			if(c==',') p++;			
		}
	}

	// set tag data
	csv->nColumns=nColumns;
	csv->sColumns=sColumns;

	return 0;
}

// csv_decode_values
// Call to decode csv values

static int csv_decode_values(CSV *csv)
{
	// mark start of data
	char *p=csv->pParse;

	// count rows
	int nRows=0;

	if(*p!=0)
	{
		do nRows++;
		while(csv_next_line(csv)==0);
	}

	// must have rows
	if(nRows==0)
	{
		engine_error("no csv rows found");
		return -1;
	}

	// allocate grid
	char **sValues=(char **) malloc(sizeof(char *)*csv->nColumns*nRows);

	if(sValues==NULL)
	{
		engine_error("out of memory allocating csv values");
		return -1;
	}

	// set csv data
	char c;

	int nRow;
	int nColumn;

	for(nRow=0;nRow<nRows;nRow++)
	{
		// out of data?
		if(*p==0)
		{
			free(sValues);

			engine_error("not enough data for csv values");
			return -1;
		}

		// get column data for row
		for(nColumn=0;nColumn<csv->nColumns;nColumn++)
		{
			// set data pointer
			sValues[(nRow*csv->nColumns)+nColumn]=p;

			// find next column
			while((c=*p)!=0 && c!=',' && c!='\n' && c!='\r')
			{
				p++;

				// skip string literals
				if(c=='\"' || c=='\'')
				{
					while(*p!=0 && *p!=c)
					{
						if(*p=='\\') p++;
						if(*p!=0) p++;
					}
					if(*p!=0) p++;
				}
			}
			*p=0;

			if(c!=0) p++;
		}

		// skip to next line
		while((c=*p)!=0 && (c=='\n' || c=='\r')) p++;
	}

	// set csv data
	csv->nRows=nRows;
	csv->sValues=sValues;

	return 0;
}

// csv_next_line
// Call to get next line

static int csv_next_line(CSV *csv)
{
	char c;

	while((c=*csv->pParse)!=0 && c!='\r' && c!='\n')
		csv->pParse++;

	return csv_skip_blank(csv);
}

// csv_skip_blank
// Call to skip blank lines

static int csv_skip_blank(CSV *csv)
{
	char c;

	while((c=*csv->pParse)!=0 && (c=='\r' || c=='\n'))
		csv->pParse++;

	return (c==0)?-1:0;
}
