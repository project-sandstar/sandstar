// value.c

// Include files

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "global.h"
#include "engine.h"
#include "table.h"
#include "value.h"

// Local functions

static double lerp(double a,double b,double f);

// Public functions

// value_status
// Call to set value to status

int value_status(ENGINE_VALUE *value,ENGINE_STATUS status)
{
	// set unknown status
	value->status=status;

	value->raw=(ENGINE_DATA) -1;
	value->cur=(ENGINE_DATA) -1;

	value->flags=0;
	value->trigger=0;

	return 0;
}

// value_raw
// Call to set value raw data

int value_raw(ENGINE_VALUE *value,ENGINE_DATA raw)
{
	// set raw value
	value->raw=raw;

	value->flags=ENGINE_DATA_RAW;
	value->trigger=0;

	return 0;
}

// value_cur
// Call to set value cur data

int value_cur(ENGINE_VALUE *value,ENGINE_DATA cur)
{
	// set cur value
	value->cur=cur;

	value->flags=ENGINE_DATA_CUR;
	value->trigger=0;

	return 0;
}

// value_convert
// Call to set cur value from raw value

int value_convert(ENGINE_VALUE *value,VALUE_CONV *conv,TABLE *tables)
{
	// get raw value
	double raw=(double) value->raw;

	double low=conv->low;
	double high=conv->high;

	double min=conv->min;
	double max=conv->max;

	// clamp raw to low/high?
	if((conv->flags & VALUE_CONV_USELOW) && raw<low) raw=low;
	else if((conv->flags & VALUE_CONV_USEHIGH) && raw>high) raw=high;

	// convert from raw to cur
	double cur=raw;

	// analog to digital conv?
	if(conv->flags & VALUE_CONV_ADC)
	{
		// convert to digital
		double l=(conv->flags & VALUE_CONV_USELOW)?low:0;
		double h=(conv->flags & VALUE_CONV_USEHIGH)?high:4096;

		cur=(cur>=(h-l)*0.5)?1.0:0.0;
	}
	else
	{
		// using table?
		if(conv->nTable>=0)
		{
			// must have high/low tags
			if((conv->flags & VALUE_CONV_USERANGE)!=VALUE_CONV_USERANGE)
			{
				value_status(value,ENGINE_STATUS_FAULT);
				return -1;
			}

			// lowest?
			if(raw<=low) cur=min;
			else
			{
				// highest?
				if(raw>=high) cur=max;
				else
				{
					// locate in table
					TABLE_ITEM *item=&tables->items[conv->nTable];

					TABLE_VALUE find=(TABLE_VALUE) raw;
					double unit=(max-min)/((double) item->nValues);

					int n;

					for(n=0;n<item->nValues-1;n++)
					{
						TABLE_VALUE r1=item->values[n];
						TABLE_VALUE r2=item->values[n+1];

						double index=(double) n;

						double c1=min+(index*unit);
						double c2=c1+unit;

						// check table direction
						if(item->nDirection>0)
						{
							// found table entry?
							if(find>=r1 && find<r2)
							{
								// interpolate rest
								double l=(find-r1)/(r2-r1);
								cur=lerp(c1,c2,l);

								break;
							}
						}
						else
						{
							// found table entry?
							if(find<=r1 && find>r2)
							{
								// interpolate rest
								double l=(find-r2)/(r1-r2);
								cur=lerp(c2,c1,l);

								break;
							}
						}
					}
				}
			}
		}
		else
		{
			// scale value within range?
			if((conv->flags & VALUE_CONV_USERANGE)==VALUE_CONV_USERANGE)
				cur/=high-low;
		}
	}

	// scale and offset?
	if(conv->flags & VALUE_CONV_USEOFFSET) cur+=conv->offset;
	if(conv->flags & VALUE_CONV_USESCALE) cur*=conv->scale;

	// clamp cur to min/max?
	if((conv->flags & VALUE_CONV_USEMIN) && cur<min) cur=min;
	else if((conv->flags & VALUE_CONV_USEMAX) && cur>max) cur=max;

	// set cur value
	value->status=ENGINE_STATUS_OK;

	value->raw=(ENGINE_DATA) raw;
	value->cur=(ENGINE_DATA) cur;

	value->flags|=ENGINE_DATA_CUR;
	value->trigger=0;

	return 0;
}

// value_revert
// Call to set raw value from cur value

int value_revert(ENGINE_VALUE *value,VALUE_CONV *conv,TABLE *tables)
{
	// get cur value
	double cur=(double) value->cur;

	double low=conv->low;
	double high=conv->high;

	double min=conv->min;
	double max=conv->max;

	// clamp to min/max?
	if((conv->flags & VALUE_CONV_USEMIN) && cur<min) cur=min;
	else if((conv->flags & VALUE_CONV_USEMAX) && cur>max) cur=max;

	// convert from cur to raw
	double raw=cur;

	// remove scale and offset?
	if(conv->flags & VALUE_CONV_USESCALE) raw/=conv->scale;
	if(conv->flags & VALUE_CONV_USEOFFSET) raw+=conv->offset;

	// analog to digital conv?
	if(conv->flags & VALUE_CONV_ADC)
	{
		// convert to digital
		double l=(conv->flags & VALUE_CONV_USELOW)?low:0;
		double h=(conv->flags & VALUE_CONV_USEHIGH)?high:4096;

		raw=(raw<=0.0)?l:h;
	}
	else
	{
		// using table?
		if(conv->nTable>=0)
		{
			// must have high/low tags
			if((conv->flags & VALUE_CONV_USERANGE)!=VALUE_CONV_USERANGE)
			{
				value_status(value,ENGINE_STATUS_FAULT);
				return -1;
			}

			// lowest?
			if(raw<=min) raw=low;
			else
			{
				// highest?
				if(raw>=max) raw=high;
				else
				{
					// locate in table
					TABLE_ITEM *item=&tables->items[conv->nTable];

					TABLE_VALUE find=(TABLE_VALUE) raw;
					double unit=(max-min)/((double) item->nValues);

					int n;

					for(n=0;n<item->nValues-1;n++)
					{
						TABLE_VALUE r1=item->values[n];
						TABLE_VALUE r2=item->values[n+1];

						double index=(double) n;

						double c1=min+(index*unit);
						double c2=c1+unit;

						// check table direction
						if(item->nDirection>0)
						{
							// found table entry?
							if(find>=c1 && find<c2)
							{
								// interpolate rest
								double l=(find-c1)/(c2-c1);
								raw=(int) lerp(r1,r2,l);

								break;
							}
						}
						else
						{
							// found table entry?
							if(find>=c1 && find<c2)
							{
								// interpolate rest
								double l=(find-c2)/(c1-c2);
								raw=(int) lerp(r2,r1,l);

								break;
							}
						}
					}
				}
			}
		}
		else
		{
			// remove value range?
			if((conv->flags & VALUE_CONV_USERANGE)==VALUE_CONV_USERANGE)
				raw*=high-low;
		}
	}

	// clamp to low/high?
	if((conv->flags & VALUE_CONV_USELOW) && raw<low) raw=low;
	else if((conv->flags & VALUE_CONV_USEHIGH) && raw>high) raw=high;

	// set raw value
	value->status=ENGINE_STATUS_OK;

	value->raw=(ENGINE_DATA) raw;
	value->cur=(ENGINE_DATA) cur;

	value->flags|=ENGINE_DATA_RAW;
	value->trigger=0;

	return 0;
}

// value_cmp
// Call to compare two values

int value_cmp(ENGINE_VALUE *value1,ENGINE_VALUE *value2)
{
	// status or cur value different?
	if(value1->status==value2->status &&
		value1->cur==value2->cur)
		return 0;

	return -1;
}

// Local functions

// lerp
// Call for linear interpolation

static double lerp(double a,double b,double f) 
{
	return (a*(1.0-f))+(b*f);
}
