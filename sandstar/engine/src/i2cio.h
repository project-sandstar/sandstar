// i2cio.h

#ifndef __I2CIO_H__
#define __I2CIO_H__

// Typedefs

typedef unsigned int I2CIO_DEVICE;
typedef unsigned int I2CIO_ADDRESS;

typedef unsigned short I2CIO_VALUE;

// Public functions

int i2cio_exists(I2CIO_DEVICE device,I2CIO_ADDRESS address);

int i2cio_get_measurement(I2CIO_DEVICE device,I2CIO_ADDRESS address,I2CIO_VALUE *value);

#endif // __I2CIO_H__
