// poll.c

// Include files

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "global.h"
#include "engine.h"
#include "table.h"
#include "value.h"
#include "channel.h"
#include "watch.h"
#include "notify.h"
#include "poll.h"

// Local functions

static int poll_find(POLL *poll,ENGINE_CHANNEL channel);
static int poll_alloc(POLL *poll);

// Public functions

// poll_init
// Call to initialize poll controller

int poll_init(POLL *poll,int nItems)
{
	int nSize=nItems*sizeof(POLL_ITEM);

	poll->nItems=nItems;
	poll->nCount=0;

	poll->items=(POLL_ITEM *) malloc(nSize);
	memset(poll->items,0,nSize);

	return 0;
}

// poll_exit
// Call to release poll controller

int poll_exit(POLL *poll)
{
	// free items?
	if(poll->items!=NULL)
	{
		free(poll->items);
		poll->items=NULL;
	}

	poll->nItems=0;
	poll->nCount=0;

	return 0;
}

// poll_add
// Call to add item to poll list

int poll_add(POLL *poll,ENGINE_CHANNEL channel)
{
	// already polling?
	int n=poll_find(poll,channel);
	if(n>=0) return 0;

	// get next free
	n=poll_alloc(poll);

	if(n<0)
	{
		engine_error("out of poll space");
		return -1;
	}

	// enable poll item
	POLL_ITEM *item=&poll->items[n];

	item->nUsed=1;
	item->channel=channel;

	// initial value
	value_status(&item->value,ENGINE_STATUS_UNKNOWN);

	// add to count
	poll->nCount++;

	return 0;
}

// poll_remove
// Call to remove polled item

int poll_remove(POLL *poll,ENGINE_CHANNEL channel)
{
	// already polling?
	int n=poll_find(poll,channel);

	if(n>=0)
	{
		// mark as unused
		poll->items[n].nUsed=0;
		poll->nCount--;

		return 0;
	}
	return -1;
}

// poll_watch
// Call to update initial watch

int poll_watch(POLL *poll,ENGINE_CHANNEL channel,key_t sender)
{
	// find poll?
	int n=poll_find(poll,channel);

	if(n>=0)
	{
		// send current
		POLL_ITEM *item=&poll->items[n];
		return watch_send(sender,item->channel,&item->value);
	}
	return -1;
}

// poll_notify
// Call to update initial notify

int poll_notify(POLL *poll,key_t sender)
{
	// push initial items
	int n;

	for(n=0;n<poll->nItems;n++)
	{
		POLL_ITEM *item=&poll->items[n];

		// item in use?
		if(item->nUsed!=0)
			notify_send(sender,item->channel,&item->value);
	}
	return 0;
}

// poll_update
// Call to update current values

int poll_update(POLL *poll,CHANNEL *channels,TABLE *tables,WATCH *watch,NOTIFY *notify)
{
	// poll current items
	int n;

	for(n=0;n<poll->nItems;n++)
	{
		POLL_ITEM *item=&poll->items[n];

		// item in use?
		if(item->nUsed!=0)
		{
			// read channel value?
			ENGINE_VALUE value;
			channel_read(channels,item->channel,&value,tables);

			// value has changed?
			if(value_cmp(&item->value,&value)!=0)
			{
				// update observers
				watch_update(watch,item->channel,&value);
				notify_update(notify,item->channel,&value);

				// keep new value
				item->value=value;
			}
		}
	}
	return 0;
}

// poll_report
// Call to display list of active polls

int poll_report(POLL *poll,CHANNEL *channels,TABLE *tables)
{
	static char *sStatus[]={"ok","unknown","stale","disable","fault","down"};

	// output header
	printf("\nchannel type            dir     table            status  raw     cur\n");
	printf("------- --------------- ------- ---------------- ------- ------- -------\n");

	// output polled items
	int n,c;

	for(n=0;n<poll->nItems;n++)
	{
		POLL_ITEM *item=&poll->items[n];

		// item in use?
		if(item->nUsed!=0)
		{
			// find channel entry
			ENGINE_CHANNEL channel=item->channel;

			for(c=0;c<channels->nItems;c++)
			{
				CHANNEL_ITEM *channelItem=&channels->items[c];

				if(channelItem->channel==channel)
				{
					int nTable=channelItem->conv.nTable;

					printf("%-7d %-15.15s %-7.7s %-16.16s %-7.7s %-7g %g\n",
						channel,
						g_sChannelType[(int) channelItem->type],
						g_sChannelDirection[(int) channelItem->direction],
						(nTable>=0)?tables->items[nTable].sTag:"n/a",
						sStatus[(int) item->value.status],
						item->value.raw,
						item->value.cur);

					break;
				}
			}
		}
	}
	printf("\n");

	return 0;
}

// Local functions

// poll_find
// Call to find existing poll item

static int poll_find(POLL *poll,ENGINE_CHANNEL channel)
{
	int n;

	for(n=0;n<poll->nItems;n++)
	{
		// item found?
		POLL_ITEM *item=&poll->items[n];

		if(item->nUsed!=0 && item->channel==channel)
			return n;
	}
	return -1;
}

// poll_alloc
// Call to find first free poll item

static int poll_alloc(POLL *poll)
{
	int n;

	for(n=0;n<poll->nItems;n++)
	{
		// item not in use?
		if(poll->items[n].nUsed==0)
			return n;
	}
	return -1;
}
