// notify.h

#ifndef __NOTIFY_H__
#define __NOTIFY_H__

// Definitions

#define MAX_NOTIFIES	8

// Notify item

struct _NOTIFY_ITEM
{
	int nUsed;
	key_t sender;
};

typedef struct _NOTIFY_ITEM NOTIFY_ITEM;

// Notify struct

struct _NOTIFY
{
	int nItems;
	int nCount;

	NOTIFY_ITEM *items;
};

typedef struct _NOTIFY NOTIFY;

// Public functions

int notify_init(NOTIFY *notify,int nItems);
int notify_exit(NOTIFY *notify);

int notify_add(NOTIFY *notify,key_t sender);
int notify_remove(NOTIFY *notify,key_t sender);

int notify_send(key_t sender,ENGINE_CHANNEL channel,ENGINE_VALUE *value);
int notify_update(NOTIFY *notify,ENGINE_CHANNEL channel,ENGINE_VALUE *value);

int notify_report(NOTIFY *notify);

#endif // __NOTIFY_H__
