// pwmscan.c

// Include files

#include <unistd.h>
#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <dirent.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>

// Definitions

#define PWMIO_OCPFS		"/sys/devices/platform/ocp"

// Local functions

static const char *io_ext(const char *sDevice);
static int display_error(char *sFormat,...);

// Public functions

// main
// Called at program startup

int main(int argc,char *argv[])
{
	// find param address?
	char *sAddr=(argc>1)?argv[1]:NULL;

	int found=0;
	int count=0;

	// open platform ocp?
	DIR *d1=opendir(PWMIO_OCPFS);

	if(d1==NULL)
	{
		display_error("cant open %s",PWMIO_OCPFS);
		return -1;
	}

	// read each entry
	struct dirent *e1;

	while(found==0 && (e1=readdir(d1))!=NULL)
	{
		// extension match pwm ss?
		const char *ext1=io_ext(e1->d_name);

		if(strcmp(ext1,"epwmss")==0)
		{
			// path to sub dir
			char dir2[256];
			sprintf(dir2,PWMIO_OCPFS "/%s",e1->d_name);

			// open sub dir?
			DIR *d2=opendir(dir2);

			if(d2==NULL)
			{
				display_error("cant open %s",dir2);
				break;
			}

			// read each entry
			struct dirent *e2;

			while(found==0 && (e2=readdir(d2))!=NULL)
			{
				// extension match pwm?
				const char *ext2=io_ext(e2->d_name);

				if(strcmp(ext2,"pwm")==0 || strcmp(ext2,"ecap")==0)
				{
					// get device address
					char addr2[10];
					sscanf(e2->d_name,"%8s",addr2);

					// addresses match?
					if(sAddr==NULL || strcmp(sAddr,addr2)==0)
					{
						// path to pwm dir
						char dir3[256];
						sprintf(dir3,"%s/%s/pwm",dir2,e2->d_name);

						// path to pwm dir
						DIR *d3=opendir(dir3);

						if(d3==NULL)
						{
							display_error("cant open %s",dir3);
							break;
						}

						// read each entry
						struct dirent *e3;

						while(found==0 && (e3=readdir(d3))!=NULL)
						{
							// found chip entry?
							if(strstr(e3->d_name,"pwmchip")==e3->d_name)
							{
								// get channel count
								char sDevice[256];
								char sCount[16];

								sprintf(sDevice,"%s/%s/npwm",dir3,e3->d_name);
								strcpy(sCount,"unknown");

								int hDevice=open(sDevice,O_RDONLY);

								if(hDevice>=0)
								{
									int nSize=read(hDevice,sCount,15);

									if(nSize>=0)
									{
										int n;

										for(n=0;n<nSize;n++)
											if(sCount[n]<32) sCount[n]=0;

										sCount[n]=0;
									}

									close(hDevice);
								}

								// output header?
								if(count==0)
								{
									printf("\nAddress  Chip     Channels\n");
									printf("-------- -------- --------\n");
								}

								// output details
								printf("%s %s %s\n",addr2,e3->d_name,sCount);
								if(sAddr!=NULL) found=1;

								count++;
							}
						}

						// close pwm dir
						closedir(d3);
					}
				}
			}

			// close sub dir
			closedir(d2);
		}
	}

	// close ocp dir
    closedir(d1);

	// found address?
	if(sAddr!=NULL && found==0)
	{
		display_error("no chip at that address");
		return -1;
	}

	// found anything?
	if(count==0)
	{
		display_error("no pwm devices found");
		return -1;
	}

	printf("\n");

	return 0;
}

// io_ext
// Call to get file extension of device

static const char *io_ext(const char *sDevice)
{
	const char *dot=strrchr(sDevice,'.');
	if(dot==NULL || dot==sDevice) return "";

	return dot+1;
}

// display_error
// Call to display engine error

static int display_error(char *sFormat,...)
{
    va_list args;

	// output error
	printf("pwmscan: error: ");

	va_start(args,sFormat);
	vprintf(sFormat,args);
	va_end(args);

	printf("\n");

	return -1;
}
